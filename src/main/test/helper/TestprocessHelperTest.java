package helper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import bo.Teststep;
import jUnitHelper.TestObjectHelper;

public class TestprocessHelperTest {

    private Teststep teststep;

    @Before
    public void setup() {
	teststep = TestObjectHelper.createTeststep();
    }

    @Test
    public void test_countFulfilled() {

	// No subcriterion is fulfilled or irrelevant.

	Assert.assertEquals(0, TestprocessHelper.countFulfilled(teststep));

	// 4 subcriteria are fulfilled, none is irrelevant.

	teststep.getTestcriteria().get(0).getTestsubcriteria().get(0).setFulfilled(true);
	teststep.getTestcriteria().get(0).getTestsubcriteria().get(2).setFulfilled(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(4).setFulfilled(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(6).setFulfilled(true);

	Assert.assertEquals(4, TestprocessHelper.countFulfilled(teststep));

	// 4 subcriteria are fulfilled, 2 of them are irrelevant.

	teststep.getTestcriteria().get(0).getTestsubcriteria().get(2).setIrrelevant(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(4).setIrrelevant(true);

	Assert.assertEquals(2, TestprocessHelper.countFulfilled(teststep));

	// 4 subcriteria are fulfilled, 4 of them are irrelevant.

	teststep.getTestcriteria().get(0).getTestsubcriteria().get(0).setIrrelevant(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(6).setIrrelevant(true);

	Assert.assertEquals(0, TestprocessHelper.countFulfilled(teststep));
    }

    @Test
    public void test_countRelevant() {

	// All subcriteria are relevant

	Assert.assertEquals(15, TestprocessHelper.countRelevant(teststep));

	// 4 subcriteria are irrelevant.

	teststep.getTestcriteria().get(0).getTestsubcriteria().get(0).setIrrelevant(true);
	teststep.getTestcriteria().get(0).getTestsubcriteria().get(2).setIrrelevant(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(4).setIrrelevant(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(6).setIrrelevant(true);

	Assert.assertEquals(11, TestprocessHelper.countRelevant(teststep));
    }
}
