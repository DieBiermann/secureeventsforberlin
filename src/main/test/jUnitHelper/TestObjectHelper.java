package jUnitHelper;

import java.util.Arrays;

import bo.Testcriterion;
import bo.Teststep;
import bo.Testsubcriterion;
import builder.TestcriterionBuilder;
import builder.TeststepBuilder;
import builder.TestsubcriterionBuilder;

/**	A helper class for jUnit test, that creates fully-fledged test-objects
 * 	@author Annina Biermann
 */
public class TestObjectHelper {

    /**	Creates a fully-fledged {@link Teststep} with {@link Testcriterion} objects and {@link Testsubcriterion}. 
     * 	@return	A fully-fledged {@link Teststep} object.
     */
    public static Teststep createTeststep() {

	// CREATES THE CRITERION EVENTCONCEPT AND ITS SUBCRITERIA

	Testcriterion eventconcept = new TestcriterionBuilder() //
		.setTestcriterionname("Veranstaltungskonzept") //
		.build();

	Testsubcriterion eventData = createTestsubcriterion(eventconcept, "Veranstaltungsdaten", 1);
	Testsubcriterion eventZone = createTestsubcriterion(eventconcept, "Veranstaltungsbereich", 2);
	Testsubcriterion visitors = createTestsubcriterion(eventconcept, "Besucher/Teilnehmer", 2);
	Testsubcriterion eventDescription = createTestsubcriterion(eventconcept, "Veranstaltungsart/Beschreibung", 1);
	Testsubcriterion eventProcess = createTestsubcriterion(eventconcept, "Veranstaltungsablauf", 1);

	eventconcept.setTestsubcriteria(Arrays.asList(eventData, eventZone, visitors, eventDescription, eventProcess));

	// CREATES THE CRITERION SAFETYCONCEPT

	Testcriterion safetyconcept = new TestcriterionBuilder() //
		.setTestcriterionname("Sicherheitskonzept") //
		.build();

	Testsubcriterion emergencyRoutes = createTestsubcriterion(safetyconcept, "Rettungswege", 2);
	Testsubcriterion medicalService = createTestsubcriterion(safetyconcept, "Sanitätsdienst", 2);
	Testsubcriterion fireProtection = createTestsubcriterion(safetyconcept, "Brandschutz", 2);
	Testsubcriterion visitorGuiding = createTestsubcriterion(safetyconcept, "Besucherführung", 2);
	Testsubcriterion constructions = createTestsubcriterion(safetyconcept, "Aufbauten", 1);
	Testsubcriterion coordinatingPoint = createTestsubcriterion(safetyconcept, "Koordinierungsstelle", 1);
	Testsubcriterion communication = createTestsubcriterion(safetyconcept, "Kommunikation", 1);
	Testsubcriterion damageScenario = createTestsubcriterion(safetyconcept, "Schadensszenarien", 2);
	Testsubcriterion maps = createTestsubcriterion(safetyconcept, "Kartenmaterial", 1);
	Testsubcriterion parallelEvents = createTestsubcriterion(safetyconcept, "Parallelveranstaltungen", 1);

	safetyconcept.setTestsubcriteria(Arrays.asList(emergencyRoutes, medicalService, fireProtection, visitorGuiding,
		constructions, coordinatingPoint, communication, damageScenario, maps, parallelEvents));

	// CREATES THE TESTSTEP WITH EVENT- AND SAFETYCONCEPT AS CHILDREN

	Teststep teststep = new TeststepBuilder() //
		.setTeststepname("Teil Aa - Vorprüfung") //
		.setTestcriteria(Arrays.asList(eventconcept, safetyconcept)).build();

	eventconcept.setParentTeststep(teststep);

	return teststep;
    }

    private static Testsubcriterion createTestsubcriterion(Testcriterion parent, String name, int score) {
	return new TestsubcriterionBuilder() //
		.setParentTestcriterion(parent) //
		.setTestsubcriterionname(name) //
		.setScore(score) //
		.build();

    }
}
