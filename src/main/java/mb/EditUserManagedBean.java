package mb;

import java.io.Serializable;
import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import bo.User;
import helper.FacesHelper;
import provider.OutcomeStatusProvider;
import services.ActionService;
import services.FinderService;
import services.ServiceFactory;
import services.UserService;

/**	Managed Bean, that is responsible editing existing user object and updates it in the persistence layer.
 * 	@author	Annina Biermann
 */
@ManagedBean
@ViewScoped
public class EditUserManagedBean implements Serializable {
    private static final long serialVersionUID = 4191546971823177321L;

    /**	An action service for logging and finding actions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();
    /**	A user service for user related interactions.	*/
    private transient UserService userService = ServiceFactory.createUserService();

    /**	The ID of the edited user.	*/
    private Integer userId;
    /**	The edited user.	*/
    private User editedUser;

    /**	Finds the user related to the commited ID.
     * 	If no user could be found, the visitor is returned to the event overview.
     * 	@see		faces-config
     * 	@return	The redirection, depending on a successful ID resolving.
     */
    public String onLoad() {
	if (getUserId() != null) {
	    Optional<User> optUser = FinderService.findById(User.class, getUserId());
	    if (optUser.isPresent()) {
		setEditedUser(optUser.get());
		return "";
	    }
	}
	return OutcomeStatusProvider.FAIL;
    }

    /**	Validates and updates the edited user and passes it to the persistence layer.
     * 	@see		faces-config
     * 	@return	The redirection, depending on a successful validation/updating.
     */
    public String editUser() {
	Optional<String> validationMessage = userService.validateEditedUser(editedUser);
	if (!validationMessage.isPresent()) {
	    userService.updateUser(editedUser);
	    actionService.logUserEditing(getEditedUser());
	    return OutcomeStatusProvider.SUCCESS;
	}
	FacesHelper.createErrorMessage(validationMessage.get(), "editedUserMessages");
	return "";
    }

    // GETTER AND SETTER

    public Integer getUserId() {
	return userId;
    }

    public void setUserId(Integer userId) {
	this.userId = userId;
    }

    public User getEditedUser() {
	return editedUser;
    }

    public void setEditedUser(User editedUser) {
	this.editedUser = editedUser;
    }
}
