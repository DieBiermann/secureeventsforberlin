package mb;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

import bo.ArchivedEvent;
import helper.FacesHelper;
import services.ActionService;
import services.FinderService;
import services.PersistenceService;
import services.ServiceFactory;

/**	Managed Bean, that is responsible for regulating archived events.
 * 	@author	Annina Biermann
 */
@ManagedBean
@RequestScoped
public class ArchivedEventManagedBean {

    /**	An event service for database related interactions.	*/
    private transient PersistenceService persistenceService = ServiceFactory.createPersistenceService();

    /**	An action service for logging and finding actions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();

    /**	Deletes an archived event from the database.
     */
    public void deleteEvent() {
	ArchivedEvent selectedEvent = FacesHelper.getSelectedObject(ArchivedEvent.class);
	if (selectedEvent != null) {
	    persistenceService.delete(selectedEvent);
	    actionService.logEventDeletion(selectedEvent);
	}
    }

    // GETTER AND SETTER

    public List<ArchivedEvent> getArchivedEvents() {
	return FinderService.findAllData(ArchivedEvent.class);
    }
}
