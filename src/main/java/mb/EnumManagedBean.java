package mb;

import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import enums.ActionType;
import enums.EditingSuggestion;
import enums.EventStatus;
import enums.Permission;

/**	Managed Bean to make specific enum objects accessable to the EL.
 * 	@author 	Annina Biermann
 */
@ManagedBean
@RequestScoped
public class EnumManagedBean {

    // ACTION TYPES

    public ActionType getTypeObjCreated() {
	return ActionType.OBJECT_CREATED;
    }

    public ActionType getTypeObjEdited() {
	return ActionType.OBJECT_EDITED;
    }

    public ActionType getTypeObjDeleted() {
	return ActionType.OBJECT_DELETED;
    }

    public ActionType getTypeEventRejected() {
	return ActionType.EVENT_REJECTED;
    }

    public ActionType getTypeEventRevived() {
	return ActionType.EVENT_REVIVED;
    }

    public ActionType getTypeEventApproved() {
	return ActionType.EVENT_APPROVED;
    }

    public ActionType getTypeEventCancelled() {
	return ActionType.EVENT_CANCELLED;
    }

    public ActionType getTypeEventArchived() {
	return ActionType.EVENT_ARCHIVED;
    }

    public ActionType getTypeEventContinued() {
	return ActionType.EVENT_CONTINUED;
    }

    // EDITING SUGGESTIONS

    public EditingSuggestion getNoEditingSuggestion() {
	return EditingSuggestion.NO_EDITING;
    }

    public EditingSuggestion getGeneralEditingSuggestion() {
	return EditingSuggestion.GENERAL_EDITING;
    }

    public EditingSuggestion getContinuingEditingSuggestion() {
	return EditingSuggestion.CONTINUING_EDITING;
    }

    // EVENT STATUS

    public EventStatus getUnderExamination() {
	return EventStatus.UNDER_EXAMINATION;
    }

    public EventStatus getRejected() {
	return EventStatus.REJECTED;
    }

    public EventStatus getApproved() {
	return EventStatus.APPROVED;
    }

    public EventStatus getCancelled() {
	return EventStatus.CANCELLED;
    }

    public List<EventStatus> getAllEventStatus() {
	return Arrays.asList(EventStatus.values());
    }

    // PERMISSIONS

    public List<Permission> getAllPermissions() {
	return Arrays.asList(Permission.values());
    }
}
