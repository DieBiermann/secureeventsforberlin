package mb;

import java.io.Serializable;
import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import bo.Testcriterion;
import bo.Teststep;
import bo.Testsubcriterion;
import helper.FacesHelper;
import helper.TestprocessHelper;
import provider.OutcomeStatusProvider;
import services.ActionService;
import services.FinderService;
import services.ServiceFactory;
import services.TeststepService;

/**	A Managed Bean responsible for testcriterion and testsubcriterion related user interactions,
 * 	mostly during the test process.
 * 	@author	Annina Biermann
 */
@ManagedBean
@ViewScoped
public class TestcriterionManagedBean implements Serializable {
    private static final long serialVersionUID = -6796878892006061492L;

    /**	An action service for logging and finding actions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();
    /**	An event service for teststep related interactions.	*/
    private transient TeststepService teststepService = new TeststepService();

    /**	The ID of the teststep, that is shown in the view.	*/
    private int currentTeststepId;
    /**	The teststep, that is shown in the view.	*/
    private Teststep currentTeststep;

    /**	The new testcriterion of the teststep.	*/
    private Testcriterion newTestcriterion = new Testcriterion();

    /**	The testcriterion, that is selected by the user.	*/
    private Testcriterion selectedTestcriterion;
    /**	The new testsubcriterion of one if the testcriteria.	*/
    private Testsubcriterion newTestsubcriterion = new Testsubcriterion();

    /**	Finds the teststep related to the commited ID.
     * 	If no teststep could be found, the user is returned to the event overview.
     * 	@see	faces-config
     * 	@return	The redirection, depending on a successful ID resolving.
     */
    public String onLoad() {
	Optional<Teststep> optTeststep = FinderService.findById(Teststep.class, getCurrentTeststepId());
	if (optTeststep.isPresent()) {
	    currentTeststep = TestprocessHelper.removeDuplicates(optTeststep.get());
	    return "";
	}
	return OutcomeStatusProvider.FAIL;
    }

    /**	Updates the current teststep in the persistence layer.
     */
    public void updateTeststep() {
	teststepService.updateTeststep(currentTeststep);
    }

    /**	Adds a testcriterion to the current teststep, after a successful validation and passes it to the persistence layer.
     * 	@return	The redirection, depending on a successful validation/creation.
     */
    public String addTestcriterion() {
	newTestcriterion.setModel(true);
	teststepService.addTestcriterionToTeststep(currentTeststep, newTestcriterion);
	actionService.logTestcriterionCreation(newTestcriterion);
	return "pruefschritt?faces-redirect=true&pruefschrittId=" + currentTeststepId;
    }

    /**	Adds a testsubcriterion to the selectes testcriterion, after a successful validation and passes it to the persistence layer.
     * 	@return	The redirection, depending on a successful validation/creation.
     */
    public String addTestsubcriterion() {
	Optional<String> validationMessage = teststepService.validateTestsubcriterion(newTestsubcriterion);
	if (!validationMessage.isPresent()) {
	    newTestsubcriterion.setModel(true);
	    teststepService.addTestsubcriterionToTestcriterion(selectedTestcriterion, newTestsubcriterion);
	    actionService.logTestsubcriterionCreation(newTestsubcriterion);
	    return "pruefschritt?faces-redirect=true&pruefschrittId=" + currentTeststepId;
	}
	FacesHelper.createErrorMessage(validationMessage.get(), "newSubcriterionForm");
	return "";
    }

    /**	Deletes the selected testcriterion.
     */
    public String deleteTestcriterion() {
	teststepService.deleteTestcriterionstep(selectedTestcriterion);
	actionService.logTestcriterionDeletion(selectedTestcriterion);
	return "pruefschritt?faces-redirect=true&pruefschrittId=" + currentTeststepId;
    }

    // GETTER AND SETTER

    public int getCurrentTeststepId() {
	return currentTeststepId;
    }

    public void setCurrentTeststepId(int currentTeststepId) {
	this.currentTeststepId = currentTeststepId;
    }

    public Teststep getCurrentTeststep() {
	return currentTeststep;
    }

    public void setCurrentTeststep(Teststep currentTeststep) {
	this.currentTeststep = currentTeststep;
    }

    public Testcriterion getNewTestcriterion() {
	return newTestcriterion;
    }

    public void setNewTestcriterion(Testcriterion newTestcriterion) {
	this.newTestcriterion = newTestcriterion;
    }

    public Testcriterion getSelectedTestcriterion() {
	return selectedTestcriterion;
    }

    public void setSelectedTestcriterion(Testcriterion selectedTestcriterion) {
	this.selectedTestcriterion = selectedTestcriterion;
    }

    public Testsubcriterion getNewTestsubcriterion() {
	return newTestsubcriterion;
    }

    public void setNewTestsubcriterion(Testsubcriterion newTestsubcriterion) {
	this.newTestsubcriterion = newTestsubcriterion;
    }
}
