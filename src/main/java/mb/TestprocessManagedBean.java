package mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import bo.Event;
import bo.Teststep;
import bo.Testsubcriterion;
import enums.EditingSuggestion;
import helper.TestprocessHelper;
import provider.OutcomeStatusProvider;
import services.ActionService;
import services.EventService;
import services.FinderService;
import services.ServiceFactory;
import services.TestprocessService;
import services.TeststepService;

/**	A Managed Bean responsible for testprocess related user interactions.
 * 	@author	Annina Biermann
 */
@ManagedBean
@ViewScoped
public class TestprocessManagedBean implements Serializable {
    private static final long serialVersionUID = -784562966927922117L;

    /**	An action service for logging and finding actions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();
    /**	An testprocess service for testprocess related interactions.	*/
    private transient TestprocessService testprocessService = ServiceFactory.createTestprocessService();
    /**	An testprocess service for teststep related interactions.	*/
    private transient TeststepService teststepService = ServiceFactory.createTeststepService();
    /**	An event service for event related interactions.	*/
    private transient EventService eventService = ServiceFactory.createEventService();

    /**	The ID of the event currently shown in the view.	*/
    private Integer eventId;
    /**	The event currently shown in the view.	*/
    private Event event;

    /**	The current calculated editing suggestion.	*/
    private EditingSuggestion currentEditingSuggestion;
    /**	The current teststep of the event.	*/
    private Teststep currentTeststep;

    /**	Initialized the attributes of the Managed Bean.
     * 	If no event could be found or another error occurs, the user is returned to the event overview.
     * 	@see		faces-config
     * 	@return	The redirection, depending on a successful ID resolving.
     */
    public String onLoad() {
	Optional<Event> optEvent = FinderService.findById(Event.class, eventId);
	if (optEvent.isPresent()) {
	    setEvent(optEvent.get());
	    setCurrentTeststep(testprocessService.findCurrentTeststep(getEvent().getTestprocess()));
	    updateSuggestion();
	    return "";
	}
	return OutcomeStatusProvider.FAIL;
    }

    /**	Saves a subcriterion that is marked checked/unchecked on the event.
     * 	@param 	testsubcriterion	the subcriterion, that is selected	
     */
    public void saveFulfilled(Testsubcriterion testsubcriterion) {
	updateSuggestion();
	if (testsubcriterion.isFulfilled()) {
	    actionService.logTestsubcriterionMarkedFulfilled(event, testsubcriterion);
	} else {
	    actionService.logTestsubcriterionMarkedNotFulfilled(event, testsubcriterion);
	}
	teststepService.updateTestsubcriterion(testsubcriterion);
    }

    /**	Saves a subcriterion that is marked relevant/irrelecant on the event.
     * 	@param 	testsubcriterion	the subcriterion, that is selected	
     */
    public void saveIrrelevant(Testsubcriterion testsubcriterion) {
	updateSuggestion();
	if (testsubcriterion.isIrrelevant()) {
	    actionService.logTestsubcriterionMarkedIrrelevant(event, testsubcriterion);
	} else {
	    actionService.logTestsubcriterionMarkedRelevant(event, testsubcriterion);
	}
	teststepService.updateTestsubcriterion(testsubcriterion);
    }

    /**	Takes the event to the next teststep.
     * 	@return	The URL of the next teststep in the testprocess wizard.
     */
    public String nextStep() {
	teststepService.fulfillTeststep(currentTeststep);
	actionService.logEventContinued(event, currentTeststep);
	return "pruefprozessassistent?faces-redirect=true&eventId=" + eventId;
    }

    /**	Sets the status of the event to rejected.
     * 	@see		faces-config
     * 	@return	The reference to the event overview.
     */
    public String rejectEvent() {
	eventService.rejectEvent(event);
	actionService.logEventRejected(event);
	return OutcomeStatusProvider.SUCCESS;
    }

    /**	Sets the status of the event to approved.
     * 	@see		faces-config
     * 	@return	The reference to the event overview.
     */
    public String approveEvent() {
	eventService.approveEvent(event);
	actionService.logEventApproved(event);
	return OutcomeStatusProvider.SUCCESS;
    }

    /**	Sets the status of the event to cancelled.
     * 	@see		faces-config
     * 	@return	The reference to the event overview.
     */
    public String cancelEvent() {
	eventService.cancelEvent(event);
	actionService.logEventCancelled(event);
	return OutcomeStatusProvider.SUCCESS;
    }

    /**	Calculates the rate of the current event and returns the fullfilled / count of all subcriteria rate as a string.
     * 	@return	The fullfilled / count of all subcriteria rate as a string.
     */
    public String calculateRate() {
	return String.valueOf(TestprocessHelper.calculateRate(currentTeststep));
    }

    /**	Depending on the calculated rate, a editing suggestion is evaluated.
     * 	@return	The evaluated suggestion.
     */
    public String getEditingSuggestionLabel() {
	return getCurrentEditingSuggestion().getSuggestionLabel();
    }

    /**	Depending on the editing suggestion, a style class for the editing suggestion is returned.
     * 	@see		The testprocess.css file in the resources.
     * 	@return	A style class for the editing suggestion is returned.
     */
    public String getEditingSuggestionStyleClass() {
	return getCurrentEditingSuggestion().getStyleClass();
    }

    /**	
     * 	@return	A boolean, if the current event is in the last teststep.
     */
    public boolean isLastStep() {
	List<Teststep> teststeps = new ArrayList<>();
	teststeps.addAll(event.getTestprocess().getTeststeps());
	return teststeps.indexOf(currentTeststep) == (teststeps.size() - 1);
    }

    /**	Updates the editing suggestion.
     */
    private void updateSuggestion() {
	currentEditingSuggestion = TestprocessHelper.resolveEditingSuggestion(currentTeststep);
    }

    // GETTER AND SETTER

    public Integer getEventId() {
	return eventId;
    }

    public void setEventId(Integer eventId) {
	this.eventId = eventId;
    }

    public Event getEvent() {
	return event;
    }

    public void setEvent(Event event) {
	this.event = event;
    }

    public Teststep getCurrentTeststep() {
	return currentTeststep;
    }

    public void setCurrentTeststep(Teststep currentTeststep) {
	this.currentTeststep = currentTeststep;
    }

    public EditingSuggestion getCurrentEditingSuggestion() {
	return currentEditingSuggestion;
    }
}