package mb;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang3.time.DateUtils;

import bo.Event;
import helper.FacesHelper;
import provider.OutcomeStatusProvider;
import services.EventService;
import services.FinderService;
import services.ServiceFactory;
import services.UserService;

/**	Managed Bean, that is responsible for editing an existing event object and updates it in the persistence layer.
 * 	@author	Annina Biermann
 */
@ManagedBean
@ViewScoped
public class EditEventManagedBean implements Serializable {
    private static final long serialVersionUID = -6950866155174464530L;

    /**	An event service for event related interactions.	*/
    private transient EventService eventService = ServiceFactory.createEventService();
    /**	An user service for user related interactions.	*/
    private transient UserService userService = ServiceFactory.createUserService();

    /**	The ID of the event, that is edited.	*/
    private Integer eventId;
    /**	The event, that is edited.	*/
    private Event editedEvent;
    /**	The selected username from the dropdown.	*/
    private String usernameOfEditor;

    /**	Finds the event related to the commited ID.
     * 	If no event could be found, the user is returned to the event overview.
     * 	@see		faces-config
     * 	@return	The redirection, depending on a successful ID resolving.
     */
    public String onLoad() {
	Optional<Event> optEvent = FinderService.findById(Event.class, eventId);
	if (optEvent.isPresent()) {
	    editedEvent = optEvent.get();
	    setUsernameOfEditor();
	    return "";
	}
	return OutcomeStatusProvider.FAIL;
    }

    /**	Validates and updates the edited event and passes it to the persistence layer.
     * 	@see		faces-config
     * 	@return	The redirection, depending on a successful validation/updating.
     */
    public String editEvent() {
	Optional<String> validationMessage = eventService.validateEvent(editedEvent);
	if (!validationMessage.isPresent()) {
	    editedEvent.setUser(userService.findUserByUsername(usernameOfEditor).get());
	    eventService.updateEvent(editedEvent);
	    return OutcomeStatusProvider.SUCCESS;
	}
	FacesHelper.createErrorMessage(validationMessage.get(), "editedEventMessages");
	return "";
    }

    /**	Sets the username of the editor string, that's needed for the 
     * 	dropdown menue. 
     * 	The editor of an event can be null, e.g. when it is deleted.
     */
    private void setUsernameOfEditor() {
	if (editedEvent.getUser() != null) {
	    usernameOfEditor = editedEvent.getUser().getUsername();
	}
    }

    // GETTER AND SETTER

    public Date getCurrentDate() {
	return DateUtils.truncate(new Date(), Calendar.DATE);
    }

    public Integer getEventId() {
	return eventId;
    }

    public void setEventId(Integer eventId) {
	this.eventId = eventId;
    }

    public Event getEditedEvent() {
	return editedEvent;
    }

    public void setEditedEvent(Event newEvent) {
	this.editedEvent = newEvent;
    }

    public String getUsernameOfEditor() {
	return usernameOfEditor;
    }

    public void setUsernameOfEditor(String usernameOfEditor) {
	this.usernameOfEditor = usernameOfEditor;
    }
}
