package mb;

import java.io.Serializable;
import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import bo.User;
import helper.FacesHelper;
import provider.OutcomeStatusProvider;
import services.ActionService;
import services.ServiceFactory;
import services.UserService;

/**	Managed Bean, that is responsible for creating a new user object and passes it to the persistence layer.
 * 	@author	Annina Biermann
 */
@ManagedBean
@ViewScoped
public class CreateUserManagedBean implements Serializable {
    private static final long serialVersionUID = 480486690550419758L;

    /**	An action service for logging and finding actions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();
    /**	A user service for user related interactions.	*/
    private transient UserService userService = ServiceFactory.createUserService();

    /**	The new user the data from the front-end is given to.	*/
    private User newUser = new User();
    /**	The password for the new user. It needs to be encoded, before it is passed to the new user object. */
    private String password;
    /**	The password repetition.	*/
    private String passwordRepetition;

    /**	Validates and creates a the new user and passes it to the persistence layer.
     * 	@return	The redirection, depending on a successful validation/creation.
     */
    public String createUser() {
	Optional<String> validateMessage = userService.validateNewUserInput(newUser, password, passwordRepetition);
	if (!validateMessage.isPresent()) {
	    userService.createUser(newUser, password);
	    actionService.logUserCreation(newUser);
	    return OutcomeStatusProvider.SUCCESS;
	}
	FacesHelper.createErrorMessage(validateMessage.get(), "newUserMessages");
	return "";
    }

    // GETTER AND SETTER

    public User getNewUser() {
	return newUser;
    }

    public void setNewUser(User newUser) {
	this.newUser = newUser;
    }

    public String getPasswordRepetition() {
	return passwordRepetition;
    }

    public void setPasswordRepetition(String passwordRepetition) {
	this.passwordRepetition = passwordRepetition;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }
}
