package mb;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import bo.User;
import enums.Permission;
import helper.FacesHelper;
import services.ActionService;
import services.FinderService;
import services.ServiceFactory;
import services.UserService;

/**	A Managed Bean responsible for user related user interactions.
 * 	@author	Annina Biermann
 */
@ManagedBean
@RequestScoped
public class UserManagedBean {

    /**	An action service for logging and finding actions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();
    /**	An user service for user related interactions.	*/
    private transient UserService userService = ServiceFactory.createUserService();

    /**	Redirects to the user editing view and passes the user ID to the view.
     * 	@return	The redirect URL.
     */
    public String editUser() {
	User selectedUser = FacesHelper.getSelectedObject(User.class);
	return "benutzer/bearbeiten?faces-redirect=true&userId=" + selectedUser.getId();
    }

    /**	Deletes the selected user off the persistence layer.
     * 	@return	The reference to the same page.
     */
    public String deleteUser() {
	User selectedUser = FacesHelper.getSelectedObject(User.class);
	ServiceFactory.createUserService().deleteUser(selectedUser);
	actionService.logUserDeletion(selectedUser);
	return "";
    }

    /**	Returns all users, except the one, that is currently logged in.
     * 	It prevents administrators from deleting themselves.
     * 	@return	All users, exept the one, that is currently logged in.
     */
    public List<User> getAllUsersExceptCurrent() {
	Integer currentUserId = FacesHelper.findSessionUser().getId();
	return FinderService.findAllData(User.class).stream() //
		.filter(u -> u.getId() != currentUserId) //
		.collect(Collectors.toList());
    }

    /**	
     * 	@return	Usernames of users, that have the writing permission.	
     */
    public List<String> getAllUsernamesWithWritingPermission() {
	List<String> usernames = new ArrayList<String>();
	for (User user : userService.findUsersWithPermission(Permission.WRITE)) {
	    usernames.add(user.getUsername());
	}
	return usernames;
    }
}
