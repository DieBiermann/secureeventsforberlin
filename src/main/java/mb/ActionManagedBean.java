package mb;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import bo.Action;
import services.ActionService;
import services.ServiceFactory;

/**	Managed Bean, that is responsible for getting executed actions from the persistence layer.
 * 	@author	Annina Biermann
 */
@ManagedBean
@RequestScoped
public class ActionManagedBean {

    /**	A user service for action related interactions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();

    /**	
     * 	@return	All actions from the persistence layer.
     */
    public List<Action> getActions() {
	return actionService.findActionsOrderedByExecutionDate();
    }
}
