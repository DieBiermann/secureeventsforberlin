package mb;

import java.util.Date;
import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import bo.Event;
import helper.FacesHelper;
import helper.TimeHelper;
import provider.OutcomeStatusProvider;
import services.ActionService;
import services.EventService;
import services.ServiceFactory;
import services.UserService;

/**	Managed Bean, that is responsible for creating a new event object and passes it to the persistence layer.
 * 	@author	Annina Biermann
 */
@ManagedBean
@RequestScoped
public class CreateEventManagedBean {

    /**	An action service for logging and finding actions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();
    /**	An event service for event related interactions.	*/
    private transient EventService eventService = ServiceFactory.createEventService();
    /**	An user service for user related interactions.	*/
    private transient UserService userService = ServiceFactory.createUserService();

    /**	The new event the data from the front-end is given to.	*/
    private Event newEvent = new Event();
    /**	The selected username from the dropdown.	*/
    private String usernameOfEditor;

    /**	Validates and creates a the new event and passes it to the persistence layer.
     * 	@see		faces-config
     * 	@return	The redirection, depending on a successful validation/creation.
     */
    public String createEvent() {
	Optional<String> validateMessage = eventService.validateEvent(newEvent);
	if (!validateMessage.isPresent()) {
	    newEvent.setUser(userService.findUserByUsername(usernameOfEditor).get());
	    eventService.createEvent(newEvent);
	    actionService.logEventCreation(newEvent);
	    return OutcomeStatusProvider.SUCCESS;
	}
	FacesHelper.createErrorMessage(validateMessage.get(), "validateMessage");
	return "";
    }

    public Date getCurrentDate() {
	return TimeHelper.getCurrentTime();
    }

    // GETTER AND SETTER

    public Event getNewEvent() {
	return newEvent;
    }

    public void setNewEvent(Event newEvent) {
	this.newEvent = newEvent;
    }

    public String getUsernameOfEditor() {
	return usernameOfEditor;
    }

    public void setUsernameOfEditor(String usernameOfEditor) {
	this.usernameOfEditor = usernameOfEditor;
    }
}
