package mb;

import java.io.Serializable;
import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import enums.Permission;
import helper.FacesHelper;
import provider.OutcomeStatusProvider;
import services.ServiceFactory;
import services.UserService;

/**	Managed Bean, that is responsible for regulating the login and logout of a user.
 * 	It is session scoped, since the current user is needed as long as the session exists.
 * 	@author	Annina Biermann
 */
@ManagedBean(eager = true)
@SessionScoped
public class LoginManagedBean implements Serializable {
    private static final long serialVersionUID = -1381101568311500915L;

    /**	The ID of the user, that is currently logged in.	*/
    private Integer currentUserId;
    /**	A boolean flag, if a user is logged in in the current session.	*/
    private boolean loggedIn = false;

    /**	Credentials for attempts to login.	*/
    private String username;
    private String password;

    /**	Processes an attempt to login. If 
     * 		- the given user is not existing
     * 		- the given password is invalid
     * 	an error message is created and the user remains on the login-page.
     * 	Else the user is successfully authenticated.
     * 	@return	The outcome of the login validation.
     */
    public String login() {
	UserService userService = ServiceFactory.createUserService();
	Optional<String> validateMessage = userService.validateCredentials(username, password);
	if (!validateMessage.isPresent()) {
	    setCurrentUserId(userService.findUserByUsername(username).get().getId());
	    loggedIn = true;
	    return OutcomeStatusProvider.SUCCESS;
	}
	FacesHelper.createErrorMessage(validateMessage.get(), "loginFormMessages");
	return "";
    }

    /**	Deletes the current user from the session and redirects to the login-page.
     * 	@return	The outcome of the logout.
     */
    public String logout() {
	setCurrentUserId(null);
	loggedIn = false;
	FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	return OutcomeStatusProvider.SUCCESS;
    }

    public boolean getHasUserWritingPermission() {
	return ServiceFactory.createUserService().hasUserPermission(currentUserId, Permission.WRITE);
    }

    public boolean getHasUserAdminPermission() {
	return ServiceFactory.createUserService().hasUserPermission(currentUserId, Permission.ADMINISTRATE);
    }

    // GETTER AND SETTER

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public Integer getCurrentUserId() {
	return currentUserId;
    }

    public void setCurrentUserId(Integer currentUserId) {
	this.currentUserId = currentUserId;
    }

    public boolean isLoggedIn() {
	return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
	this.loggedIn = loggedIn;
    }
}
