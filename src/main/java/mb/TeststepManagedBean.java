package mb;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import bo.Testprocess;
import bo.Teststep;
import helper.FacesHelper;
import provider.OutcomeStatusProvider;
import services.ActionService;
import services.ServiceFactory;
import services.TeststepService;

/**	A Managed Bean responsible for teststep related user interactions.
 * 	@author	Annina Biermann
 */
@ManagedBean
@ViewScoped
public class TeststepManagedBean implements Serializable {
    private static final long serialVersionUID = -3443851318986297127L;

    /**	An action service for logging and finding actions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();
    /**	An event service for teststep related interactions.	*/
    private transient TeststepService teststepService = new TeststepService();

    /**	The testprocess model	*/
    private Testprocess testprocessModel;
    /**	The new teststep	*/
    private Teststep newTeststep = new Teststep();

    /**	Find the testprocess model, if existend in the persistence layer.
     * 	Else create a new model.
     */
    public void onLoad() {
	testprocessModel = ServiceFactory.createTestprocessService().findOrCreateTestprocessModel();
    }

    /**	Adds a teststep to the testprocess model.
     * 	@see	faces-config
     * 	@return	The reference to the teststep overview.
     */
    public String addTeststep() {
	newTeststep.setModel(true);
	teststepService.addTeststepToTestprocess(testprocessModel, newTeststep);
	actionService.logTeststepCreation(newTeststep);
	return OutcomeStatusProvider.SUCCESS;
    }

    /**	Deletes the selected teststep from the testprocess model.
     * 	@see		faces-config
     * 	@return	The reference to the teststep overview.
     */
    public String deleteTeststep() {
	Teststep selectedTeststep = FacesHelper.getSelectedObject(Teststep.class);
	teststepService.deleteTeststep(selectedTeststep);
	actionService.logTeststepDeletion(selectedTeststep);
	return OutcomeStatusProvider.SUCCESS;
    }

    /**	Redirects to the testcriterion editing view and passes the teststep ID to the view.
     * 	@return	The redirect URL.
     */
    public String openTeststep() {
	return "pruefschritte/pruefschritt?faces-redirect=true&pruefschrittId=" + FacesHelper.getSelectedObject(Teststep.class).getId();
    }

    // GETTER AND SETTER

    public Testprocess getTestprocessModel() {
	return testprocessModel;
    }

    public void setTestprocessModel(Testprocess testprocessModel) {
	this.testprocessModel = testprocessModel;
    }

    public Teststep getNewTeststep() {
	return newTeststep;
    }

    public void setNewTeststep(Teststep newTeststep) {
	this.newTeststep = newTeststep;
    }
}
