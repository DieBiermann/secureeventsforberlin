package mb;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import bo.Event;
import enums.EventStatus;
import helper.FacesHelper;
import services.ActionService;
import services.EventService;
import services.ServiceFactory;

/**	A Managed Bean responsible for event related user interactions.
 * 	@author	Annina Biermann
 */
@ManagedBean(eager = true)
@ViewScoped
public class EventManagedBean implements Serializable {
    private static final long serialVersionUID = -1401794265492568212L;

    /**	An action service for logging and finding actions.	*/
    private transient ActionService actionService = ServiceFactory.createActionService();
    /**	An event service for event related interactions.	*/
    private transient EventService eventService = ServiceFactory.createEventService();

    /**	The selected event filter on the event overview.	*/
    private EventStatus selectedEventStatus = EventStatus.UNDER_EXAMINATION;

    /**	Redirects to the testprocess view and passes the event ID to the view.
     * 	@return	The redirect URL.
     */
    public String openInTestProcessWizard() {
	Event selectedEvent = FacesHelper.getSelectedObject(Event.class);
	return "veranstaltungen/pruefprozessassistent?faces-redirect=true&eventId=" + selectedEvent.getId();
    }

    /**	Redirects to the event editing view and passes the event ID to the view.
     * 	@return	The redirect URL.
     */
    public String editEvent() {
	Event selectedEvent = FacesHelper.getSelectedObject(Event.class);
	return "veranstaltungen/bearbeiten?faces-redirect=true&eventId=" + selectedEvent.getId();
    }

    /**	Sets the status of the selected event to archived.
     * 	@return	The redirect URL to the same page.
     */
    public String archiveEvent() {
	Event selectedEvent = FacesHelper.getSelectedObject(Event.class);
	eventService.archiveEvent(selectedEvent);
	actionService.logEventArchived(selectedEvent);
	return null;
    }

    /**	Sets the status of the selected event to under examination.
     * 	@return	The redirect URL to the same page.
     */
    public String reviveEvent() {
	Event selectedEvent = FacesHelper.getSelectedObject(Event.class);
	eventService.reviveEvent(selectedEvent);
	actionService.logEventRevived(selectedEvent);
	return null;
    }

    /**	Returns the events depending on the choosen status from the filter dropdown.
     * 	@return	The events depending on the choosen status.
     */
    public List<Event> getFilteredEvents() {
	return eventService.findEventsWithStatus(selectedEventStatus);
    }

    // GETTER AND SETTER

    public EventStatus getSelectedEventStatus() {
	return selectedEventStatus;
    }

    public void setSelectedEventStatus(EventStatus selectedEventStatus) {
	this.selectedEventStatus = selectedEventStatus;
    }
}