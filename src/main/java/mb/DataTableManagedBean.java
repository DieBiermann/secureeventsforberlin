package mb;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.html.HtmlDataTable;

import org.primefaces.component.datatable.DataTable;

/**	The {@link HtmlDataTable} cannot be bound to a Managed Bean, that has a bigger Scope than {@link RequestScoped}.
 * 	That is why the binding and accessing has to be outsourced.
 * 	@author Anna
 */
@ManagedBean
@RequestScoped
public class DataTableManagedBean {
    /**	the primefaces datatable	*/
    private transient DataTable primeFacesDataTable;
    /**	the normal JSF datatable	*/
    private transient HtmlDataTable htmlDataTable;

    /**	Returns the selected object from the primefaces/JSF datatable.
     * 	@param 	clazz	the class the selected object is casted to
     * 	@return	The selected object from the primefaces/JSF datatable.
     */
    @SuppressWarnings("unchecked")
    public <T> T getSelectedElement(Class<T> clazz) {
	if (primeFacesDataTable != null) {
	    return (T) primeFacesDataTable.getRowData();
	}
	return (T) htmlDataTable.getRowData();
    }

    // GETTER AND SETTER

    public HtmlDataTable getHtmlDataTable() {
	return htmlDataTable;
    }

    public void setHtmlDataTable(HtmlDataTable htmlDataTable) {
	this.htmlDataTable = htmlDataTable;
    }

    public DataTable getPrimeFacesDataTable() {
	return primeFacesDataTable;
    }

    public void setPrimeFacesDataTable(DataTable primeFacesDataTable) {
	this.primeFacesDataTable = primeFacesDataTable;
    }
}
