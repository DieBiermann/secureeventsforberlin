package services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import jpa.PersistenceManager;

/**	A  finder service for generic finder requests for the database.
 * 	@author	Annina Biermann
 */
public class FinderService {

    /**	Finds the element of the given class with the given ID.
     * 	Returns null, when the ID could not be found.
     * 	@param 	clazz	the searched for class
     * 	@param 	id		the searched for ID
     * 	@return	The found element or nothing.
     */
    public static <T> Optional<T> findById(Class<T> clazz, Integer id) {
	EntityManager entityManager = PersistenceManager.getEntityManager();
	try {
	    if (id != null) {
		T t = entityManager.find(clazz, id);
		if (t != null) {
		    return Optional.of(t);
		}
	    }
	} catch (Exception e) {
	    System.out.println("[ERROR] - An error occured while getting an object of the instance " + clazz.getName() + " with the ID " + id
		    + " from the database: " + e);
	} finally {
	    entityManager.close();
	}
	return Optional.empty();
    }

    /**	Finds all persisted objects of a class.
     * 	@param 	clazz	the searched for class
     * 	@return	All persisted objects of a class.	
     */
    public static <T> List<T> findAllData(Class<T> clazz) {
	EntityManager entityManager = PersistenceManager.getEntityManager();
	List<T> objects = new ArrayList<T>();
	try {
	    objects = entityManager.createQuery("Select a From " + clazz.getSimpleName() + " a", clazz).getResultList();
	} catch (Exception e) {
	    System.out.println("[ERROR] - An error occured while getting all data of the instance " + clazz.getName() + " : " + e);
	} finally {
	    entityManager.close();
	}
	return objects;
    }
}
