package services;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import bo.Testprocess;
import bo.Teststep;
import helper.TestprocessHelper;
import jpa.PersistenceManager;

/**	A service for testprocess related interactions.
 * 	@author	Annina Biermann
 */
public class TestprocessService {

    /**	An event service for database related interactions.	*/
    private PersistenceService persistenceService = ServiceFactory.createPersistenceService();

    /**	Finds the testprocess model. If no model was found,
     * 	a new one is created and persisted.
     * 	@return	the testprocess model or nothing.
     */
    public Testprocess findOrCreateTestprocessModel() {
	Optional<Testprocess> optTestprocess = findTestprocessModel();
	if (optTestprocess.isPresent()) {
	    return optTestprocess.get();
	}
	Testprocess testprocessModel = new Testprocess();
	testprocessModel.setModel(true);
	persistenceService.save(testprocessModel);
	return testprocessModel;
    }

    /**	Finds the teststep of the given testprocess, that is currently active.
     * 	If none of the steps is fulfilled, the first step is returned.
     * 	@param	 	testprocess	the testprocess, that shall be evaluated
     * 	@return	The  currently active teststep of a testprocess.
     */
    public Teststep findCurrentTeststep(Testprocess testprocess) {
	Testprocess withoutDuplicates = TestprocessHelper.removeDuplicates(testprocess);
	Optional<Teststep> current = withoutDuplicates.getTeststeps().stream() //
		.filter(t -> !t.isFulfilled()) //
		.findFirst();
	if (current.isPresent()) {
	    return current.get();
	}
	return testprocess.getTeststeps().iterator().next();
    }

    // PRIVATE METHODS

    private Optional<Testprocess> findTestprocessModel() {
	EntityManager entityManager = PersistenceManager.getEntityManager();
	Query query = entityManager //
		.createQuery("FROM " + Testprocess.class.getName() + " e WHERE e.isModel = true");
	try {
	    Testprocess singleResult = (Testprocess) query.getSingleResult();
	    return Optional.of(TestprocessHelper.removeDuplicates(singleResult));
	} catch (NoResultException ex) {
	    return Optional.empty();
	} finally {
	    entityManager.close();
	}
    }
}
