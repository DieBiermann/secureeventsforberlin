package services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import bo.ArchivedEvent;
import bo.Event;
import bo.User;
import enums.EventStatus;
import helper.TestprocessHelper;
import jpa.PersistenceManager;
import provider.ValidationMessageProvider;

/**	Service class for event-object interactions.
 * 	@author 	Annina Biermann
 */
public class EventService {

    /**	An event service for database related interactions.	*/
    private PersistenceService persistenceService = ServiceFactory.createPersistenceService();

    /**	Creates a new event in the database. Its initial status is under examination 
     * 	and its given an empty testprocess, cloned by the model the user deposited.
     * 	@param 	event				the event to persist	
     * 	@param 	usernameOfEditor	the username of the editor
     */
    public void createEvent(Event event) {
	event.setStatus(EventStatus.UNDER_EXAMINATION);
	event.setTestprocess(TestprocessHelper.cloneTestprocessModel());
	persistenceService.save(event);
    }

    /**	Updates the event in the database.
     *		@param 	editedEvent	the event to update
     */
    public void updateEvent(Event editedEvent) {
	persistenceService.update(editedEvent);
    }

    /**	Sets the events status to approve and updates in in the database.
     * 	@param 	event	the event to approve
     */
    public void approveEvent(Event event) {
	event.setStatus(EventStatus.APPROVED);
	persistenceService.update(event);
    }

    /**	Sets the events status to reject and updates in in the database.
     * 	@param 	event	the event to reject
     */
    public void rejectEvent(Event event) {
	event.setStatus(EventStatus.REJECTED);
	persistenceService.update(event);
    }

    /**	Sets the events status to under examination and updates in in the database.
     * 	@param 	event	the event to reject
     */
    public void reviveEvent(Event event) {
	event.setStatus(EventStatus.UNDER_EXAMINATION);
	persistenceService.update(event);
    }

    /**	Sets the events status to cancelled and updates in in the database.
     * 	@param 	event	the event to cancel
     */
    public void cancelEvent(Event event) {
	event.setStatus(EventStatus.CANCELLED);
	persistenceService.update(event);
    }

    /**	Creates an {@link ArchivedEvent} from an existing event and persists it.
     * 	The event is deleted afterwards, since archived is its last status.
     * 	@param 	event	the event to archive
     */
    public void archiveEvent(Event event) {
	ArchivedEvent archivedEvent = new ArchivedEvent();
	archivedEvent.setEventname(event.getEventname());
	archivedEvent.setStart(event.getStart());
	archivedEvent.setEnd(event.getEnd());
	archivedEvent.setLastStatus(event.getStatus());
	persistenceService.save(archivedEvent);
	persistenceService.delete(event);
    }

    /**	Finds the events, that are currently in the given status.
     * 	@param 	status	the status, the events should be in
     * 	@return	The events in the given status.
     */
    @SuppressWarnings("unchecked")
    public List<Event> findEventsWithStatus(EventStatus status) {
	EntityManager entityManager = PersistenceManager.getEntityManager();
	List<Event> result = new ArrayList<Event>();
	try {
	    Query query = entityManager //
		    .createQuery("FROM " + Event.class.getName() + " e WHERE LOWER(e.status) = LOWER(:status) ORDER BY e.start ASC");
	    query.setParameter("status", status.name());
	    result = query.getResultList();
	} catch (Exception e) {
	    System.out.println("[ERROR] - An error occured while fetching all events with the status " + status.toString() + ":  " + e);
	} finally {
	    entityManager.close();
	}
	return result;
    }

    /**	Finds the events, the user is currently the editor of.
     * 	@param 	user		the searched for user
     * 	@return	The events, the user is currently the editor of or an empty list.
     */
    @SuppressWarnings("unchecked")
    public List<Event> findEventsWithUser(User user) {
	EntityManager entityManager = PersistenceManager.getEntityManager();
	List<Event> result = new ArrayList<Event>();
	try {
	    Query query = entityManager //
		    .createQuery("SELECT e FROM " + Event.class.getName() + " e WHERE e.user = LOWER(:user)");
	    query.setParameter("user", user.getId());
	    result = query.getResultList();
	} catch (NoResultException e) {
	    System.out.println("[ERROR] - An error occured while fetching all events with the user " + user.getId() + ":  " + e);
	} finally {
	    entityManager.close();
	}
	return result;
    }

    // VALIDATION

    /**	Checks if the given event dates are valid. 
     * 	@param 	event	the event to check
     * 	@return	An optional Error-Message, if start- or enddate are invalid.
     */
    public Optional<String> validateEvent(Event event) {
	if (event.getStart().before(event.getEnd())) {
	    return Optional.empty();
	}
	return Optional.of(ValidationMessageProvider.EVENT_END_BEFORE_START);
    }
}
