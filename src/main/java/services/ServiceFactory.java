package services;

/**	A service factory, that helps creating certain services in a quick way.
 * 	This service should be relaced by an dependency injection container, like Spring.
 * 	@author		Annina Biermann
 */
public class ServiceFactory {

    public static ActionService createActionService() {
	return new ActionService();
    }

    public static EventService createEventService() {
	return new EventService();
    }

    public static FinderService createFinderService() {
	return new FinderService();
    }

    public static PersistenceService createPersistenceService() {
	return new PersistenceService();
    }

    public static TestprocessService createTestprocessService() {
	return new TestprocessService();
    }

    public static TeststepService createTeststepService() {
	return new TeststepService();
    }

    public static UserService createUserService() {
	return new UserService();
    }
}
