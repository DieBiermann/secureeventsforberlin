package services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import bo.Action;
import bo.ArchivedEvent;
import bo.Event;
import bo.Testcriterion;
import bo.Teststep;
import bo.Testsubcriterion;
import bo.User;
import enums.ActionType;
import helper.FacesHelper;
import jpa.PersistenceManager;

/**	Service class for action-object interactions.
 * 	@author 	Annina Biermann
 */
public class ActionService {

    /**	An event service for database related interactions.	*/
    private PersistenceService persistenceService = ServiceFactory.createPersistenceService();

    /**	Gets all existing actions from the database, ordered by the execution date.
     * 	@return	all taken actions, ordered by the execution date.
     */
    @SuppressWarnings("unchecked")
    public List<Action> findActionsOrderedByExecutionDate() {
	List<Action> resultList = new ArrayList<Action>();
	EntityManager entityManager = PersistenceManager.getEntityManager();
	try {
	    resultList = entityManager.createQuery("FROM " + Action.class.getName() + " e ORDER BY e.executedOn DESC").getResultList();
	} catch (Exception e) {
	    System.out.println("[ERROR] - An error occured while getting all action objects from the database: " + e);
	} finally {
	    entityManager.close();
	}
	return resultList;
    }

    // LOGGING OF EXECUTED ACTIONS

    // TESTSTEPS, -CRITERIA, -SUBCRITERIA

    public void logTeststepCreation(Teststep newTeststep) {
	createtAction(ActionType.OBJECT_CREATED, FacesHelper.findSessionUser(),
		"Der Prüfschritt " + newTeststep.getTeststepname() + " wurde angelegt.");
    }

    public void logTeststepDeletion(Teststep deletedTeststep) {
	createtAction(ActionType.OBJECT_DELETED, FacesHelper.findSessionUser(),
		"Der Prüfschritt " + deletedTeststep.getTeststepname() + " wurde gelöscht.");
    }

    public void logTestcriterionCreation(Testcriterion createdTestcriterion) {
	createtAction(ActionType.OBJECT_CREATED, FacesHelper.findSessionUser(),
		"Das Kriterium " + //
			createdTestcriterion.getTestcriterionname() + //
			" (" + createdTestcriterion.getParentTeststep().getTeststepname() + ") wurde angelegt.");
    }

    public void logTestcriterionDeletion(Testcriterion deletedTestcriterion) {
	createtAction(ActionType.OBJECT_DELETED, FacesHelper.findSessionUser(),
		"Das Kriterium " + //
			deletedTestcriterion.getTestcriterionname() + //
			" (" + deletedTestcriterion.getParentTeststep().getTeststepname() + ") " + //
			" und seine Unterkriterien wurden gelöscht.");
    }

    public void logTestsubcriterionCreation(Testsubcriterion createdTestsubcriterion) {
	createtAction(ActionType.OBJECT_CREATED, FacesHelper.findSessionUser(),
		"Das Kriterium " + //
			createdTestsubcriterion.getTestsubcriterionname() + //
			" (" + createdTestsubcriterion.getParentTestcriterion().getTestcriterionname() + ") wurde angelegt.");
    }

    // EVENTS

    public void logEventCreation(Event event) {
	createtAction(ActionType.OBJECT_CREATED, FacesHelper.findSessionUser(), "Die Veranstaltung " + event.getEventname() + " wurde angelegt.");
    }

    public void logEventEditing(Event event) {
	createtAction(ActionType.OBJECT_EDITED, FacesHelper.findSessionUser(), "Die Veranstaltung " + event.getEventname() + " wurde bearbeitet.");
    }

    public void logEventDeletion(ArchivedEvent selectedEvent) {
	createtAction(ActionType.OBJECT_DELETED, FacesHelper.findSessionUser(),
		"Die Veranstaltung " + selectedEvent.getEventname() + " wurde gelöscht.");
    }

    public void logTestsubcriterionMarkedFulfilled(Event event, Testsubcriterion testsubcriterion) {
	createtAction(ActionType.EVENT_CONTINUED, FacesHelper.findSessionUser(),
		"Das Unterkriterium " + //
			testsubcriterion.getTestsubcriterionname() + //
			" wurde an der Veranstaltung " + //
			event.getEventname() + " als erfüllt markiert.");
    }

    public void logTestsubcriterionMarkedNotFulfilled(Event event, Testsubcriterion testsubcriterion) {
	createtAction(ActionType.EVENT_CONTINUED, FacesHelper.findSessionUser(),
		"Das Unterkriterium " + //
			testsubcriterion.getTestsubcriterionname() + //
			" wurde an der Veranstaltung " + //
			event.getEventname() + " als nicht erfüllt markiert.");
    }

    public void logTestsubcriterionMarkedRelevant(Event event, Testsubcriterion testsubcriterion) {
	createtAction(ActionType.EVENT_CONTINUED, FacesHelper.findSessionUser(),
		"Das Unterkriterium " + //
			testsubcriterion.getTestsubcriterionname() + //
			" wurde an der Veranstaltung " + //
			event.getEventname() + " als relevant markiert.");
    }

    public void logTestsubcriterionMarkedIrrelevant(Event event, Testsubcriterion testsubcriterion) {
	createtAction(ActionType.EVENT_CONTINUED, FacesHelper.findSessionUser(),
		"Das Unterkriterium " + //
			testsubcriterion.getTestsubcriterionname() + //
			" wurde an der Veranstaltung " + //
			event.getEventname() + " als irrelevant markiert.");
    }

    public void logEventContinued(Event event, Teststep newTeststep) {
	createtAction(ActionType.EVENT_CONTINUED, FacesHelper.findSessionUser(),
		"Die Veranstaltung " + event.getEventname() + " wurde in den Prüfschritt " + newTeststep.getTeststepname() + " überführt.");
    }

    public void logEventRejected(Event event) {
	createtAction(ActionType.EVENT_REJECTED, FacesHelper.findSessionUser(),
		"Die Veranstaltung " + event.getEventname() + " wurde zurückgewiesen.");
    }

    public void logEventApproved(Event event) {
	createtAction(ActionType.EVENT_APPROVED, FacesHelper.findSessionUser(), "Die Veranstaltung " + event.getEventname() + " wurde genehmigt");
    }

    public void logEventCancelled(Event event) {
	createtAction(ActionType.EVENT_CANCELLED, FacesHelper.findSessionUser(),
		"Die Veranstaltung " + event.getEventname() + " wurde endgültig abgelehnt.");
    }

    public void logEventRevived(Event event) {
	createtAction(ActionType.EVENT_REVIVED, FacesHelper.findSessionUser(),
		"Die Veranstaltung " + event.getEventname() + " wurde wieder in Prüfung genommen.");
    }

    public void logEventArchived(Event event) {
	createtAction(ActionType.EVENT_ARCHIVED, FacesHelper.findSessionUser(), "Die Veranstaltung " + event.getEventname() + " wurde archiviert");
    }

    // USERS

    public void logUserCreation(User newUser) {
	createtAction(ActionType.OBJECT_CREATED, FacesHelper.findSessionUser(), "Der Benutzer " + newUser.getUsername() + " wurde angelegt.");
    }

    public void logUserEditing(User editedUser) {
	createtAction(ActionType.OBJECT_EDITED, FacesHelper.findSessionUser(), "Der Benutzer " + editedUser.getUsername() + " wurde bearbeitet.");
    }

    public void logUserDeletion(User deletedUser) {
	createtAction(ActionType.OBJECT_DELETED, FacesHelper.findSessionUser(), "Der Benutzer " + deletedUser.getUsername() + " wurde gelöscht.");
    }

    private void createtAction(ActionType actionType, User activator, String description) {
	Action action = new Action();
	action.setActionType(actionType);
	action.setDescription(description);
	action.setActivator(activator.getUsername() + " (ID: " + activator.getId() + ")");
	persistenceService.save(action);
    }
}
