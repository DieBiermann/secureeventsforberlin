package services;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import bo.Testcriterion;
import bo.Testprocess;
import bo.Teststep;
import bo.Testsubcriterion;

/**	A service for teststep, testcriterion and testsubcriterion related entities.
 * 	@author	Annina Biermann
 */
public class TeststepService {

    /**	An event service for database related interactions.	*/
    private PersistenceService persistenceService = ServiceFactory.createPersistenceService();

    /**	Updates the given teststep in the persistence layer.
     * 	@param 	teststep			the teststep to update
     */
    public void updateTeststep(Teststep teststep) {
	persistenceService.update(teststep);
    }

    /**	Updates the given testsubcriterion in the persistence layer.
     * 	If the testsubcriterion is marked as irrelevant, but is also marked as fulfilled,
     * 	fulfilled is set to false. Fulfilled testsubcriterion, that are irrelevant
     * 	must not falsfy the suggestion.
     * 	@param 	testsubcriterion	the testsubcriterion to update
     */
    public void updateTestsubcriterion(Testsubcriterion testsubcriterion) {
	if (testsubcriterion.isFulfilled() && testsubcriterion.isIrrelevant()) {
	    testsubcriterion.setFulfilled(false);
	}
	persistenceService.update(testsubcriterion);
    }

    /**	Marks the given teststep as fulfilled and updates it in the persistence layer. 
     * 	@param 	teststep	the teststep to mark as fulfilled
     */
    public void fulfillTeststep(Teststep teststep) {
	teststep.setFulfilled(true);
	persistenceService.update(teststep);
    }

    /**	Adds a teststep to the given testprocess and updates them in the persistence layer.
     * 	@param 	testprocess	the parent testprocess
     * 	@param 	teststep	the child teststep
     */
    public void addTeststepToTestprocess(Testprocess testprocessModel, Teststep teststep) {
	teststep.setParentTestprocess(testprocessModel);
	testprocessModel.getTeststeps().add(teststep);
	persistenceService.update(testprocessModel);
    }

    /**	Adds a testcriterion to the given teststep and updates them in the persistence layer.
     * 	@param 	teststep	the parent teststep
     * 	@param 	testcriterion	the child testcriterion
     */
    public void addTestcriterionToTeststep(Teststep teststep, Testcriterion testcriterion) {
	testcriterion.setParentTeststep(teststep);
	teststep.getTestcriteria().add(testcriterion);
	persistenceService.update(teststep);
    }

    /**	Adds a testsubcriterion to the given testcriterion and updates them in the persistence layer.
     * 	@param 	testcriterion		the parent testcriterion
     * 	@param 	testsubcriterion	the child testsubcriterion
     */
    public void addTestsubcriterionToTestcriterion(Testcriterion testcriterion, Testsubcriterion testsubcriterion) {
	testsubcriterion.setParentTestcriterion(testcriterion);
	persistenceService.update(testcriterion);
	persistenceService.save(testsubcriterion);

    }

    /**	Deletes the given teststep and its testcriteria from the database.
     * 	@param 		teststep	the teststep to delete
     */
    public void deleteTeststep(Teststep teststep) {
	// delete child from parent
	teststep.getParentTestprocess().getTeststeps().remove(teststep);
	persistenceService.update(teststep.getParentTestprocess());
	// delete child
	persistenceService.detach(teststep);
	persistenceService.delete(teststep);
    }

    /**	Deletes the given testcriterion and its testsubcriteria from the database.
     * 	@param 		teststep	the testcriterion to delete
     */
    public void deleteTestcriterionstep(Testcriterion testcriterion) {
	// delete child from parent
	testcriterion.getParentTeststep().getTestcriteria().remove(testcriterion);
	persistenceService.update(testcriterion.getParentTeststep());
	// delete child
	persistenceService.detach(testcriterion);
	persistenceService.delete(testcriterion);
    }

    /**	Checks if the given testsubcriterion is valid. 
     * 	@param 	testsubcriterion	the testsubcriterion to check
     * 	@return	An optional Error-Message, if name or score is empty and if the score lies between 1 and 10.
     */
    public Optional<String> validateTestsubcriterion(Testsubcriterion testsubcriterion) {
	if (!StringUtils.isBlank(testsubcriterion.getTestsubcriterionname())) {
	    if (testsubcriterion.getScore() > 0 && testsubcriterion.getScore() <= 10) {
		return Optional.empty();
	    }
	    return Optional.of("Der Punktwert muss ein ganzer Wert zwischen 1 und 10 sein!");
	}
	return Optional.of("Der Name des Unterkriteriums darf nicht leer sein!");
    }
}
