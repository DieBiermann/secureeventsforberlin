package services;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.mindrot.jbcrypt.BCrypt;

import bo.Event;
import bo.User;
import enums.Permission;
import jpa.PersistenceManager;
import provider.ValidationMessageProvider;

/**	Service class for user-object interactions.
 * 	@author 	Annina Biermann
 */
public class UserService {

    /**	An persistence service for database related interactions.	*/
    private PersistenceService persistenceService = ServiceFactory.createPersistenceService();
    /**	An event service for database related interactions.	*/
    private EventService eventService = ServiceFactory.createEventService();

    private static final String NAME_PATTERN = "^[a-zA-Z0-9ÄÜÖäüö._]*$";

    /**	Creates the user in the database with the given password. 
     * 	The password is persisted encrypted.
     * 	@param 	user			the user to persist
     * 	@param 	password	the passwort to persist encrypted
     */
    public void createUser(User user, String password) {
	user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
	persistenceService.save(user);
    }

    /**	Updates the user in the database.
     * 	@param 	user		the user to update
     */
    public void updateUser(User user) {
	persistenceService.update(user);
    }

    /**	Deletes the user off the database and removes him from the related events. 
     * 	@param 	user		the user to delete	
     */
    public void deleteUser(User user) {
	List<Event> events = eventService.findEventsWithUser(user);
	for (Event event : events) {
	    event.setUser(null);
	    persistenceService.update(event);
	}
	persistenceService.delete(user);
    }

    /**	Finds the user with the given username.
     * 	@param 	username	the searched for username
     * 	@return	The user with the given username or nothing.
     */
    public Optional<User> findUserByUsername(String username) {
	EntityManager entityManager = PersistenceManager.getEntityManager();
	Query query = entityManager //
		.createQuery("SELECT u FROM " + User.class.getName() + " u WHERE LOWER(u.username) = LOWER(:username)");
	query.setParameter("username", username);
	try {
	    return Optional.of((User) query.getSingleResult());
	} catch (NoResultException ex) {
	    return Optional.empty();
	} finally {
	    entityManager.close();
	}
    }

    /**	Finds all users from the database, that own the given permission.
     * 	@param 	permission	the permission, the users must own
     * 	@return	All users from the database, that own the given permission.
     * 	@TODO		Currently solved with Java 8 streams - should be done by the database via JPQL.
     */
    public List<User> findUsersWithPermission(Permission permission) {
	List<User> allUsers = FinderService.findAllData(User.class);
	allUsers.removeIf(new Predicate<User>() {
	    @Override
	    public boolean test(User user) {
		return !user.getPermissions().contains(permission);
	    }
	});
	return allUsers;
    }

    /**	Checks if the user related to the ID owns the given {@link Permission}.
     * 	If the ID could not be found, false is returned.
     * 	@param 	userId		the user to check
     * 	@param 	permission	the permission to check
     * 	@return	If the user related to the ID owns the given {@link Permission}.
     */
    public boolean hasUserPermission(Integer userId, Permission permission) {
	Optional<User> optUser = FinderService.findById(User.class, userId);
	if (optUser.isPresent()) {
	    return optUser.get().getPermissions().contains(permission);
	}
	return false;
    }

    // VALIDATION

    /**	Checks if given user exists and if the password is correct.
     * 	@param		username	the username to check
     * 	@param		password	the password to check
     * 	@return	An optional Error-Message, if username or password are invalid.
     */
    public Optional<String> validateCredentials(String username, String password) {
	Optional<User> optUser = findUserByUsername(username);
	if (optUser.isPresent()) {
	    if (BCrypt.checkpw(password, optUser.get().getPassword())) {
		return Optional.empty();
	    }
	}
	return Optional.of(ValidationMessageProvider.LOGIN_CREDENTIALS_INVALID);
    }

    /**	Checks if the given user is valid and if the password and -repetition are equal. 
     * 	@param 	user					the user to check
     * 	@param 	password			the passwort to check
     * 	@param	 	passwordRepetition	the passwort repetition to check
     * 	@return	An optional Error-Message, if user or passwords are invalid.
     */
    public Optional<String> validateNewUserInput(User user, String password, String passwordRepetition) {
	Optional<String> message = validateEditedUser(user);
	if (!message.isPresent()) {
	    if (password.equals(passwordRepetition)) {
		return Optional.empty();
	    }
	    return Optional.of(ValidationMessageProvider.USER_PASSWORDS_DONT_MATCH);
	}
	return message;
    }

    /**	Checks if the given user has a unique username and if the username matches the pattern.
     * 	@param 	user		the user to check
     * 	@return	An optional Error-Message, if the user is invalid.
     */
    public Optional<String> validateEditedUser(User user) {
	if (isUsernameUnique(user)) {
	    if (user.getUsername().matches(NAME_PATTERN)) {
		return Optional.empty();
	    }
	    return Optional.of(ValidationMessageProvider.USERNAME_NO_PATTERNMATCH);
	}
	return Optional.of(ValidationMessageProvider.USER_ALLREADY_EXISTS);
    }

    /**	Checks if the username of the given user is already existing in the database.
     * 	@param 	user		the user to check		
     * 	@return	true if the username is unique, else false
     */
    private boolean isUsernameUnique(User user) {
	Optional<User> duplicate = findUserByUsername(user.getUsername());
	if (duplicate.isPresent() && user.getId() != duplicate.get().getId()) {
	    return false;
	}
	return true;
    }
}
