package services;

import javax.persistence.EntityManager;

import jpa.PersistenceManager;
import jpa.Transaction;

/**	A service for CRUD operations of all entities.
 * 	@author 	Annina Biermann
 */
public class PersistenceService {

    /**	Save the entity in the database.
     * 	@param 	bo	the entity to save
     */
    public <T> void save(T bo) {
	executeTransaction(new Transaction() {
	    @Override
	    public void run(EntityManager entityManager) {
		entityManager.persist(bo);
	    }
	});
    }

    /**	Deletes the entity frome the database.
     * 	@param 	bo	the entity to delete
     */
    public <T> void delete(T bo) {
	executeTransaction(new Transaction() {
	    @Override
	    public void run(EntityManager entityManager) {
		entityManager.remove(entityManager.merge(bo));
	    }
	});
    }

    /**	Updates the entity in the database.
     * 	@param 	bo	the entity to update
     */
    public <T> void update(T bo) {
	executeTransaction(new Transaction() {
	    @Override
	    public void run(EntityManager entityManager) {
		entityManager.merge(bo);
	    }
	});
    }

    /**	Detaches the entity from the persistence context.
     * 	@param 	bo	the entity to detach
     */
    public <T> void detach(T bo) {
	executeTransaction(new Transaction() {
	    @Override
	    public void run(EntityManager entityManager) {
		entityManager.detach(bo);
	    }
	});
    }

    private void executeTransaction(Transaction runnableTransaction) {
	EntityManager em = PersistenceManager.getEntityManager();
	em.getTransaction().begin();
	runnableTransaction.run(em);
	em.getTransaction().commit();
	em.close();
	PersistenceManager.close();
    }
}
