package enums;

import java.util.Arrays;
import java.util.List;

import bo.User;

/**	A permission, a {@link User} can own.
 * 	@author	Annina Biermann
 */
public enum Permission {
    /**	A permission, that indicates, that the owning user has the permission to execute WRITING actions in the application.	*/
    WRITE("Schreibendes Recht"), //
    /**	A permission, that indicates, that the owning user has the permission to execute ADMINISTRATIVE actions in the application.	*/
    ADMINISTRATE("Administratives Recht");

    /**	The label of the permission visible for the user.	*/
    private final String permission;

    private Permission(final String permission) {
	this.permission = permission;
    }

    @Override
    public String toString() {
	return permission;
    }

    public static Permission identify(String permissionLabel) {
	for (Permission permission : Permission.values()) {
	    if (permission.toString().equals(permissionLabel)) {
		return permission;
	    }
	}
	throw new IllegalArgumentException("Invalid permission to identify: " + permissionLabel);
    }

    public static List<String> getAllPermissionsAsList() {
	return Arrays.asList( //
		WRITE.toString(), //
		ADMINISTRATE.toString() //
	);
    }
}
