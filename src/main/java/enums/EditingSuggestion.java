package enums;

import bo.Teststep;
import bo.Testsubcriterion;

/**	An editing suggestion of a {@link Teststep}, which depends on the count of fulfilled {@link Testsubcriterion}.
 * 	@author	Annina Biermann
 */
public enum EditingSuggestion {
    /**	The suggestion of no more editing of a teststep.	*/
    NO_EDITING("keine Bearbeitung", "suggestion-red"), //
    /**	The suggestion of general editing of a teststep.	*/
    GENERAL_EDITING("grundsätzliche Bearbeitung", "suggestion-yellow"), //
    /**	The suggestion of continuing editing.				*/
    CONTINUING_EDITING("weitere Bearbeitung", "suggestion-green");

    /**	The label of the suggestion, that is displayed to the user.	*/
    private final String suggestionLabel;
    /**	The style class, the suggestion is styled with.  
     *	@see	testprocess.css
     */
    private final String styleClass;

    private EditingSuggestion(String suggestionLabel, String styleClass) {
	this.suggestionLabel = suggestionLabel;
	this.styleClass = styleClass;
    }

    public String getSuggestionLabel() {
	return suggestionLabel;
    }

    public String getStyleClass() {
	return styleClass;
    }
}
