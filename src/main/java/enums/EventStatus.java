package enums;

import java.util.Arrays;
import java.util.List;

import bo.Event;

/**	The status, an {@link Event} can be in.
 * 	@author	Annina Biermann
 */
public enum EventStatus {
    /**	A status, that marks an {@link Event} as be in the testprocess - No decision about the event was made yet.	*/
    UNDER_EXAMINATION("In Prüfung"), //
    /**	A status, that marks an {@link Event} as rejected - There has to be made refinement by the event organizer, before an decision about the event can be made.	*/
    REJECTED("Zurükgewiesen"), //
    /**	A status, that marks an {@link Event} as finally approved.	*/
    APPROVED("Genehmigt"), //
    /**	A status, that marks an {@link Event} as finally cancelled.  	*/
    CANCELLED("Abgelehnt");

    /**	The label of the status visible for the user.	*/
    private final String status;

    private EventStatus(String status) {
	this.status = status;
    }

    @Override
    public String toString() {
	return status;
    }

    public static List<String> getAllEventStatusAsList() {
	return Arrays.asList( //
		UNDER_EXAMINATION.toString(), //
		REJECTED.toString(), //
		APPROVED.toString(), //
		CANCELLED.toString());
    }
}
