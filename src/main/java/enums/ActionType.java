package enums;

import bo.Action;
import bo.Testprocess;

/**	The type an executed {@link Action} can be.
 * 	@author	Annina Biermann
 */
public enum ActionType {
    /**	An object was CREATED.	*/
    OBJECT_CREATED, //
    /**	An object was DELETED.	*/
    OBJECT_DELETED, //
    /**	An object was EDITED.	*/
    OBJECT_EDITED, //
    /**	An {@link Testprocess} of an @{link Event} made progress.	*/
    EVENT_CONTINUED, //
    /**	An @{link Event} got into the {@link EventStatus} REJECTED.	*/
    EVENT_REJECTED, //
    /**	An @{link Event} got into the {@link EventStatus} APPROVED.	*/
    EVENT_APPROVED, //
    /**	An @{link Event} got into the {@link EventStatus} CANCELLED.	*/
    EVENT_CANCELLED, //
    /**	An @{link Event} was revived, that means, it returned from the {@link EventStatus} REJECTED to the {@link EventStatus} UNDER_EXAMINATION.	*/
    EVENT_REVIVED, //
    /**	An @{link Event} was archived.	*/
    EVENT_ARCHIVED //
    ;
}
