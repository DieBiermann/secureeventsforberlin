package provider;

/**	A provider for custom validation messages.
 * 	@author	Annina Biermann
 */
public class ValidationMessageProvider {

    // LOGIN

    public static final String LOGIN_CREDENTIALS_INVALID = "Der Benutzername oder das Passwort sind falsch!";

    // USER

    public static final String USER_PASSWORDS_DONT_MATCH = "Die Passwörter sind nicht gleich!";

    public static final String USER_ALLREADY_EXISTS = "Ein Benutzer mit diesem Benutzernamen existiert bereits!";
    public static final String USERNAME_NO_PATTERNMATCH = "Der Benutzername darf nur Buchstaben, Zahlen und die Sonderzeichen . und _ enthalten!";

    // EVENT

    public static final String EVENT_END_BEFORE_START = "Der Endtermin muss nach dem Starttermin liegen!";
}
