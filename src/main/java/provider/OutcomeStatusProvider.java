package provider;

/**	An outcome status provider for the faces-config.
 * 	@author	Annina Biermann
 */
public class OutcomeStatusProvider {

    public static final String SUCCESS = "success";

    public static final String FAIL = "fail";
}
