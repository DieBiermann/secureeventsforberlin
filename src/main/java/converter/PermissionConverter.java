package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import enums.Permission;

/**	A converter for {@link Permission} enum values.
 * 	@author	Annina Biermann
 */
@FacesConverter(value = "permissionConverter")
public class PermissionConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	return Permission.identify(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	Permission e = (Permission) value;
	return e.toString();
    }
}
