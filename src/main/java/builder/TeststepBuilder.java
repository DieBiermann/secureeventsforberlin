package builder;

import java.util.ArrayList;
import java.util.List;

import bo.Testcriterion;
import bo.Testprocess;
import bo.Teststep;

/**	A builder-class for {@link Teststep} objects.
 * 	@author Annina Biermann
 */
public class TeststepBuilder {

    /**	A boolean flag, if this object is a child of the the model {@link Testprocess}, all other testprocess-entities must be cloned of.	*/
    private boolean isModel = false;

    /**	The name of the teststep.	*/
    private String teststepname;

    /**	A boolean flag, if this teststep is considered fulfilled.		*/
    private boolean fulfilled = false;

    /**	A boolean flag, if this teststep allows irrelevant testcriteria.	*/
    private boolean irrelevantAllowed = true;

    /**	The testprocess, this teststep belongs to.	*/
    private Testprocess parentTestprocess;

    /**	The testcriteria, whose testsubcriteria must be fulfilled to fulfill this teststep.	*/
    private List<Testcriterion> testcriteria = new ArrayList<Testcriterion>();;

    public TeststepBuilder setModel(boolean isModel) {
	this.isModel = isModel;
	return this;
    }

    public TeststepBuilder setTeststepname(String teststepname) {
	this.teststepname = teststepname;
	return this;
    }

    public TeststepBuilder setFulfilled(boolean fulfilled) {
	this.fulfilled = fulfilled;
	return this;
    }

    public TeststepBuilder setIrrelevantAllowed(boolean irrelevantAllowed) {
	this.irrelevantAllowed = irrelevantAllowed;
	return this;
    }

    public TeststepBuilder setParentTestprocess(Testprocess parentTestprocess) {
	this.parentTestprocess = parentTestprocess;
	return this;
    }

    public TeststepBuilder setTestcriteria(List<Testcriterion> testcriteria) {
	this.testcriteria = testcriteria;
	return this;
    }

    /**	
     * 	@return	The {@link Teststep} object with the given attributes.
     */
    public Teststep build() {
	Teststep teststep = new Teststep();
	teststep.setModel(isModel);
	teststep.setTeststepname(teststepname);
	teststep.setFulfilled(fulfilled);
	teststep.setIrrelevantAllowed(irrelevantAllowed);
	teststep.setParentTestprocess(parentTestprocess);
	teststep.setTestcriteria(testcriteria);
	return teststep;
    }
}
