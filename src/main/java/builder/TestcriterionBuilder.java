package builder;

import java.util.ArrayList;
import java.util.List;

import bo.Testcriterion;
import bo.Testprocess;
import bo.Teststep;
import bo.Testsubcriterion;

/**	A builder-class for {@link Testcriterion} objects.
 * 	@author Annina Biermann
 */
public class TestcriterionBuilder {

    /**	A boolean flag, if this object is a child of the the model {@link Testprocess}, all other testprocess-entities must be cloned of.	*/
    private boolean isModel;

    /**	The name of the testcriterion.	*/
    private String testcriterionname;

    /**	The teststep, this testcriterion belongs to.	*/
    private Teststep parentTeststep;

    /**	The testsubcriteria, that must be fulfilled to fulfill the parent teststep.	*/
    private List<Testsubcriterion> testsubcriteria = new ArrayList<Testsubcriterion>();

    public TestcriterionBuilder setModel(boolean isModel) {
	this.isModel = isModel;
	return this;
    }

    public TestcriterionBuilder setTestcriterionname(String testcriterionname) {
	this.testcriterionname = testcriterionname;
	return this;
    }

    public TestcriterionBuilder setParentTeststep(Teststep parentTeststep) {
	this.parentTeststep = parentTeststep;
	return this;
    }

    public TestcriterionBuilder setTestsubcriteria(List<Testsubcriterion> testsubcriteria) {
	this.testsubcriteria = testsubcriteria;
	return this;
    }

    /**	
     * 	@return	The {@link Testcriterion} object with the given attributes.
     */
    public Testcriterion build() {
	Testcriterion testcriterion = new Testcriterion();
	testcriterion.setModel(isModel);
	testcriterion.setTestcriterionname(testcriterionname);
	testcriterion.setParentTeststep(parentTeststep);
	testcriterion.setTestsubcriteria(testsubcriteria);
	return testcriterion;
    }
}
