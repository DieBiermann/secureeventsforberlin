package builder;

import bo.Testcriterion;
import bo.Testprocess;
import bo.Testsubcriterion;

/**	A builder-class for {@link Testsubcriterion} objects.
 * 	@author Annina Biermann
 */
public class TestsubcriterionBuilder {

    /**	A boolean flag, if this object is a child of the the model {@link Testprocess}, all other testprocess-entities must be cloned of.	*/
    private boolean isModel;

    /**	The name of this testsubcriterion.	*/
    private String testsubcriterionname;

    /**	The score of this testsubcriterion. It describes the weight of this testsubcriterion, the default score is 1.	*/
    private int score = 1;

    /**	A boolean flag, if this teststep is considered irrelevant.	*/
    private boolean irrelevant = false;

    /**	A boolean flag, if this teststep is considered fulfilled.	*/
    private boolean fulfilled = false;

    /**	The testcriterion, this testsubcriterion belongs to.	*/
    private Testcriterion parentTestcriterion;

    public TestsubcriterionBuilder setModel(boolean isModel) {
	this.isModel = isModel;
	return this;
    }

    public TestsubcriterionBuilder setTestsubcriterionname(String testsubcriterionname) {
	this.testsubcriterionname = testsubcriterionname;
	return this;
    }

    public TestsubcriterionBuilder setScore(int score) {
	this.score = score;
	return this;
    }

    public TestsubcriterionBuilder setIrrelevant(boolean irrelevant) {
	this.irrelevant = irrelevant;
	return this;
    }

    public TestsubcriterionBuilder setFulfilled(boolean fulfilled) {
	this.fulfilled = fulfilled;
	return this;
    }

    public TestsubcriterionBuilder setParentTestcriterion(Testcriterion parentTestcriterion) {
	this.parentTestcriterion = parentTestcriterion;
	return this;
    }

    /**	
     * 	@return	The {@link Testsubcriterion} object with the given attributes.
     */
    public Testsubcriterion build() {
	Testsubcriterion testsubcriterion = new Testsubcriterion();
	testsubcriterion.setModel(isModel);
	testsubcriterion.setTestsubcriterionname(testsubcriterionname);
	testsubcriterion.setScore(score);
	testsubcriterion.setFulfilled(fulfilled);
	testsubcriterion.setIrrelevant(irrelevant);
	testsubcriterion.setParentTestcriterion(parentTestcriterion);
	return testsubcriterion;
    }
}
