package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mb.LoginManagedBean;

/**	A filter for unauthorized accesses to the side via deep links.
 * 	If the current session is non-existent or no user is logged in,
 * 	accesses via deep links is prohibited.
 * 	@author	Annina Biermann
 */
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

	HttpSession session = ((HttpServletRequest) request).getSession(true);
	LoginManagedBean loginManagedBean = (LoginManagedBean) session.getAttribute("loginManagedBean");

	if (loginManagedBean == null || !loginManagedBean.isLoggedIn()) {
	    String contextPath = ((HttpServletRequest) request).getContextPath();
	    ((HttpServletResponse) response).sendRedirect(contextPath + "/anmelden.html");
	} else {
	    chain.doFilter(request, response);
	}
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
	// Nothing to do here!
    }

    @Override
    public void destroy() {
	// Nothing to do here!
    }
}
