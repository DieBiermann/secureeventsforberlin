package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mb.LoginManagedBean;

/**	A filter for unauthorized accesses parts of the application,
 * 	the user has no permission for. For example, a user without
 * 	writing permission must not access the event creation.
 * 	@author	Annina Biermann
 */
public class WritingPermissionFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

	HttpSession session = ((HttpServletRequest) request).getSession(true);
	LoginManagedBean loginManagedBean = (LoginManagedBean) session.getAttribute("loginManagedBean");

	if (!loginManagedBean.getHasUserWritingPermission()) {
	    String contextPath = ((HttpServletRequest) request).getContextPath();
	    ((HttpServletResponse) response).sendRedirect(contextPath + "/authentifiziert/veranstaltungen.html");
	} else {
	    chain.doFilter(request, response);
	}
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
	// Nothing to do here!
    }

    @Override
    public void destroy() {
	// Nothing to do here!
    }
}
