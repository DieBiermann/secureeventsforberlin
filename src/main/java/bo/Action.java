package bo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import enums.ActionType;
import helper.TimeHelper;

/**	An action-object that describes interactions executed by users
 * 	while using the application.
 *	@author Annina Biermann
 */
@Entity
@Table(name = "action", schema = "sefb")
public class Action {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**	The description of the action, that was done.	*/
    @Column
    private String description;

    /**	The date and time, the action was executed on. Is set, when the object is created.	*/
    @Column
    private Date executedOn = TimeHelper.getCurrentTime();

    /**	The type of action, that was done.		*/
    @Enumerated(EnumType.STRING)
    @Column
    private ActionType actionType;

    /**	The user, that executed the action. To increase performance, only the name / id is saved.		*/
    private String activator;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public ActionType getActionType() {
	return actionType;
    }

    public void setActionType(ActionType actionType) {
	this.actionType = actionType;
    }

    public String getActivator() {
	return activator;
    }

    public void setActivator(String activator) {
	this.activator = activator;
    }

    public Date getExecutedOn() {
	return executedOn;
    }

    public void setExecutedOn(Date executedOn) {
	this.executedOn = executedOn;
    }
}
