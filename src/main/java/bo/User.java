package bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import enums.Permission;

/**	An user-object that describes a user account for authorization.
 * 	@author	Annina Biermann
 */
@Entity
@Table(name = "user", schema = "sefb")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**	The unique username of this user account.			*/
    @Column(unique = true)
    private String username;

    /**	The password of this user account.				*/
    private String password;

    /**	A list if permissioms, the user account can own.		*/
    @ElementCollection(fetch = FetchType.EAGER, targetClass = Permission.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "user_permissions")
    @Column(name = "permission")
    private List<Permission> permissions = new ArrayList<Permission>();

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public List<Permission> getPermissions() {
	return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
	this.permissions = permissions;
    }
}
