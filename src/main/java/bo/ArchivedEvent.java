package bo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import enums.EventStatus;

/**	An archived event-object that describes and event, that 
 * 	already left the testprocess and is only left for backup.
 * 	@author	Annina Biermann
 */
@Entity
@Table(name = "archivedEvent", schema = "sefb")
public class ArchivedEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**	The name of the event.			*/
    @Column
    private String eventname;

    /**	The date, the event starts on.		*/
    @Column
    private Date start;

    /**	The date, the event ends on.		*/
    @Column
    private Date end;

    /**	The last status, the event was in, when it was a fully-fledged {@link Event} Object.	*/
    @Enumerated(EnumType.STRING)
    @Column
    private EventStatus lastStatus;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getEventname() {
	return eventname;
    }

    public void setEventname(String eventname) {
	this.eventname = eventname;
    }

    public Date getStart() {
	return start;
    }

    public void setStart(Date start) {
	this.start = start;
    }

    public Date getEnd() {
	return end;
    }

    public void setEnd(Date end) {
	this.end = end;
    }

    public EventStatus getLastStatus() {
	return lastStatus;
    }

    public void setLastStatus(EventStatus lastStatus) {
	this.lastStatus = lastStatus;
    }
}
