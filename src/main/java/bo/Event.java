package bo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import enums.EventStatus;

/**	An event-object that describes and event.
 * 	@author	Annina Biermann
 */
@Entity
@Table(name = "event", schema = "sefb")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**	The name of the event.			*/
    @Column
    private String eventname;

    /**	The date, the event starts on.		*/
    @Column
    private Date start;

    /**	The date, the event ends on.		*/
    @Column
    private Date end;

    /**	The status, the event is currently in.	*/
    @Enumerated(EnumType.STRING)
    @Column
    private EventStatus status;

    /**	The user, that is currently responsible for this event.	*/
    @ManyToOne(optional = true)
    @JoinColumn(name = "user", nullable = true)
    private User user;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Testprocess testprocess;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getEventname() {
	return eventname;
    }

    public void setEventname(String eventname) {
	this.eventname = eventname;
    }

    public Date getStart() {
	return start;
    }

    public void setStart(Date start) {
	this.start = start;
    }

    public Date getEnd() {
	return end;
    }

    public void setEnd(Date end) {
	this.end = end;
    }

    public User getUser() {
	return user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public EventStatus getStatus() {
	return status;
    }

    public void setStatus(EventStatus status) {
	this.status = status;
    }

    public Testprocess getTestprocess() {
	return testprocess;
    }

    public void setTestprocess(Testprocess testprocess) {
	this.testprocess = testprocess;
    }
}
