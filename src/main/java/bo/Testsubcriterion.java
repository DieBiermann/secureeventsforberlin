package bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**	A testsubcriterion-object, that describes a single subcriterion of a {@link Testcriterion}.
 * 	It must be fulfilled to fulfill the entire {@link Teststep}.
 * 	@author	Annina Biermann
 */
@Entity
@Table(name = "testsubcriterion", schema = "sefb")
public class Testsubcriterion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**	A boolean flag, if this object is a child of the the model {@link Testprocess}, all other testprocess-entities must be cloned of.	*/
    @Column
    private boolean isModel;

    /**	The name of this testsubcriterion.	*/
    @Column(length = 1000)
    private String testsubcriterionname;

    /**	The score of this testsubcriterion. It describes the weight of this testsubcriterion, the default score is 1.	*/
    @Column
    private int score = 1;

    /**	A boolean flag, if this teststep is considered irrelevant.	*/
    @Column
    private boolean irrelevant = false;

    /**	A boolean flag, if this teststep is considered fulfilled.	*/
    @Column
    private boolean fulfilled = false;

    /**	The testcriterion, this testsubcriterion belongs to.	*/
    @ManyToOne
    private Testcriterion parentTestcriterion;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getTestsubcriterionname() {
	return testsubcriterionname;
    }

    public void setTestsubcriterionname(String testsubcriterionname) {
	this.testsubcriterionname = testsubcriterionname;
    }

    public int getScore() {
	return score;
    }

    public void setScore(int score) {
	this.score = score;
    }

    public boolean isModel() {
	return isModel;
    }

    public void setModel(boolean isModel) {
	this.isModel = isModel;
    }

    public boolean isIrrelevant() {
	return irrelevant;
    }

    public void setIrrelevant(boolean irrelevant) {
	this.irrelevant = irrelevant;
    }

    public boolean isFulfilled() {
	return fulfilled;
    }

    public void setFulfilled(boolean fulfilled) {
	this.fulfilled = fulfilled;
    }

    public Testcriterion getParentTestcriterion() {
	return parentTestcriterion;
    }

    public void setParentTestcriterion(Testcriterion parentTestcriterion) {
	this.parentTestcriterion = parentTestcriterion;
    }
}
