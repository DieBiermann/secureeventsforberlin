package bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**	A teststep-object, that describes a single step during a {@link Testprocess},
 * 	that must be fulfilled, before entering the next teststep.
 * 	@author	Annina Biermann
 */
@Entity
@Table(name = "teststep", schema = "sefb")
public class Teststep {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**	A boolean flag, if this object is a child of the the model {@link Testprocess}, all other testprocess-entities must be cloned of.	*/
    @Column
    private boolean isModel;

    /**	The name of the teststep.	*/
    @Column(length = 1000)
    private String teststepname;

    /**	A boolean flag, if this teststep is considered fulfilled.		*/
    @Column
    private boolean fulfilled = false;

    /**	A boolean flag, if this teststep allows irrelevant testcriteria.	*/
    @Column
    private boolean irrelevantAllowed = false;

    /**	The testprocess, this teststep belongs to.	*/
    @ManyToOne
    private Testprocess parentTestprocess;

    /**	The testcriteria, whose testsubcriteria must be fulfilled to fulfill this teststep.	*/
    @OneToMany(mappedBy = "parentTeststep", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Testcriterion> testcriteria = new ArrayList<Testcriterion>();

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getTeststepname() {
	return teststepname;
    }

    public void setTeststepname(String teststepname) {
	this.teststepname = teststepname;
    }

    public boolean isIrrelevantAllowed() {
	return irrelevantAllowed;
    }

    public void setIrrelevantAllowed(boolean irrelevantAllowed) {
	this.irrelevantAllowed = irrelevantAllowed;
    }

    public boolean isFulfilled() {
	return fulfilled;
    }

    public void setFulfilled(boolean fulfilled) {
	this.fulfilled = fulfilled;
    }

    public boolean isModel() {
	return isModel;
    }

    public void setModel(boolean isModel) {
	this.isModel = isModel;
    }

    public Testprocess getParentTestprocess() {
	return parentTestprocess;
    }

    public void setParentTestprocess(Testprocess parentTestprocess) {
	this.parentTestprocess = parentTestprocess;
    }

    public List<Testcriterion> getTestcriteria() {
	return testcriteria;
    }

    public void setTestcriteria(List<Testcriterion> testcriteria) {
	this.testcriteria = testcriteria;
    }
}
