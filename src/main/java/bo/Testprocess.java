package bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**	The testprocess-object, that desribes a testprocess,
 * 	an {@link Event} must go through.
 * 	@author	Annina Biermann
 */
@Entity
@Table(name = "testprocess", schema = "sefb")
public class Testprocess {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**	A boolean flag, if this object is the model object, all other testprocess-entities must be cloned of.	*/
    @Column
    private boolean isModel;

    /**	The teststeps, the {@link Event} must go through.	*/
    @OneToMany(mappedBy = "parentTestprocess", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Teststep> teststeps = new ArrayList<Teststep>();

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public boolean isModel() {
	return isModel;
    }

    public void setModel(boolean isModel) {
	this.isModel = isModel;
    }

    public List<Teststep> getTeststeps() {
	return teststeps;
    }

    public void setTeststeps(List<Teststep> teststeps) {
	this.teststeps = teststeps;
    }
}
