package bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**	A testcriterion-object, that describes a single criterion of a {@link Teststep}.
 * 	@author	Annina Biermann
 */
@Entity
@Table(name = "testcriterion", schema = "sefb")
public class Testcriterion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**	A boolean flag, if this object is a child of the the model {@link Testprocess}, all other testprocess-entities must be cloned of.	*/
    @Column
    private boolean isModel;

    /**	The name of the testcriterion.	*/
    @Column(length = 1000)
    private String testcriterionname;

    /**	The teststep, this testcriterion belongs to.	*/
    @ManyToOne
    private Teststep parentTeststep;

    /**	The testsubcriteria, that must be fulfilled to fulfill the parent teststep.	*/
    @OneToMany(mappedBy = "parentTestcriterion", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Testsubcriterion> testsubcriteria = new ArrayList<Testsubcriterion>();

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getTestcriterionname() {
	return testcriterionname;
    }

    public void setTestcriterionname(String testcriterionname) {
	this.testcriterionname = testcriterionname;
    }

    public boolean isModel() {
	return isModel;
    }

    public void setModel(boolean isModel) {
	this.isModel = isModel;
    }

    public Teststep getParentTeststep() {
	return parentTeststep;
    }

    public void setParentTeststep(Teststep parentTeststep) {
	this.parentTeststep = parentTeststep;
    }

    public List<Testsubcriterion> getTestsubcriteria() {
	return testsubcriteria;
    }

    public void setTestsubcriteria(List<Testsubcriterion> testsubcriteria) {
	this.testsubcriteria = testsubcriteria;
    }
}
