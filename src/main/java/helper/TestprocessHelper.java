package helper;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import bo.Testcriterion;
import bo.Testprocess;
import bo.Teststep;
import bo.Testsubcriterion;
import builder.TestcriterionBuilder;
import builder.TeststepBuilder;
import builder.TestsubcriterionBuilder;
import enums.EditingSuggestion;
import services.ServiceFactory;

/**	A helper for testprocess, teststep, testcriterion and testsubcriterion interactions.
 * 	@author	Annina Biermann
 */
public class TestprocessHelper {

    /**	Removes duplicates from the testprocess and its children.
     * 	WORKAROUND HIBERNATE BUG: When fetching data eagerly, hibernate's executing a join, that delivers certain duplicates!
     * 	This bug / feature has to be solved by this workaround.
     * 	@see		FAQ: https://developer.jboss.org/wiki/HibernateFAQ-AdvancedProblems
     * 	@param		testprocess	the testprocess to remove duplicates from
     * 	@return	A testprocess without duplicates .
     */
    public static Testprocess removeDuplicates(Testprocess testprocess) {
	List<Teststep> teststeps = testprocess.getTeststeps();
	for (Teststep teststep : teststeps) {
	    removeDuplicates(teststep);
	}
	testprocess.setTeststeps(new ArrayList<Teststep>(new LinkedHashSet<Teststep>(teststeps)));
	return testprocess;
    }

    /**	Removes duplicates from the teststep and its children.
     * 	WORKAROUND HIBERNATE BUG: When fetching data eagerly, hibernate's executing a join, that delivers certain duplicates!
     * 	This bug / feature has to be solved by this workaround.
     * 	@see		FAQ: https://developer.jboss.org/wiki/HibernateFAQ-AdvancedProblems
     * 	@param		teststep		the teststep to remove duplicates from
     * 	@return	A teststep without duplicates .
     */
    public static Teststep removeDuplicates(Teststep teststep) {
	List<Testcriterion> testcriteria = teststep.getTestcriteria();
	for (Testcriterion testcriterion : testcriteria) {
	    List<Testsubcriterion> testsubcriteria = testcriterion.getTestsubcriteria();
	    testcriterion.setTestsubcriteria(new ArrayList<Testsubcriterion>(new LinkedHashSet<Testsubcriterion>(testsubcriteria)));
	}
	teststep.setTestcriteria(new ArrayList<Testcriterion>(new LinkedHashSet<Testcriterion>(testcriteria)));
	return teststep;
    }

    // TESTSTEP RATE / SUGGESTION CALCULATING

    /**	Calculates the percental rate of the fulfilled score to complete count of score of testsubcriteria..
     * 	@return	The percental rate of the fulfilled score to complete count of score of testsubcriteria..
     */
    public static int calculateRate(Teststep teststep) {
	int procent = 0;
	try {
	    procent = ((countFulfilledScore(teststep)) * 100) / countRelevantScore(teststep);
	} catch (Exception e) {
	    System.out.println("Exception while calculating the test step status!");
	}
	return procent;
    }

    /**	Adds up the scores of the testcriteria, that are marked as relevant and are fulfilled.
     * 	@param 	testcriteria	the testcriteria to count
     * 	@return	The score of as relevant and fulfilled marked testcriteria.
     */
    public static int countFulfilledScore(Teststep teststep) {
	int fulfilledChildTestCriteria = 0;
	for (Testcriterion testcriterion : teststep.getTestcriteria()) {
	    for (Testsubcriterion testsubcriterion : testcriterion.getTestsubcriteria()) {
		if (testsubcriterion.isFulfilled() && !testsubcriterion.isIrrelevant()) {
		    fulfilledChildTestCriteria += testsubcriterion.getScore();
		}
	    }
	}
	return fulfilledChildTestCriteria;
    }

    /**	Adds up the scores of the testcriteria, that are marked as relevant.
     * 	@param 	testcriteria	the testcriteria to count
     * 	@return	The score of as relevant marked testcriteria.
     */
    public static int countRelevantScore(Teststep teststep) {
	int fulfilledChildTestCriteria = 0;
	for (Testcriterion testcriterion : teststep.getTestcriteria()) {
	    for (Testsubcriterion testsubcriterion : testcriterion.getTestsubcriteria()) {
		if (!testsubcriterion.isIrrelevant()) {
		    fulfilledChildTestCriteria += testsubcriterion.getScore();
		}
	    }
	}
	return fulfilledChildTestCriteria;
    }

    /**	Depending on the calculated rate, a editing suggestion is resolved.
     * 	@return	The evaluated suggestion.
     */
    public static EditingSuggestion resolveEditingSuggestion(Teststep teststep) {
	int procent = calculateRate(teststep);
	if (procent < 60) {
	    return EditingSuggestion.NO_EDITING;
	} else if (procent >= 60 && procent < 80) {
	    return EditingSuggestion.GENERAL_EDITING;
	} else {
	    return EditingSuggestion.CONTINUING_EDITING;
	}
    }

    // TESTPROCESS MODEL CLONING

    /**	Find or creates the testprocess model and clones its teststeps, testcriteria and testsubcriteria.
     * 	@return	A clone of the testprocess model.
     * 	@TODO		There has to be a better solution for this!
     */
    @Deprecated
    public static Testprocess cloneTestprocessModel() {
	Testprocess model = ServiceFactory.createTestprocessService().findOrCreateTestprocessModel();
	Testprocess clonedTestprocess = new Testprocess();
	clonedTestprocess.setModel(false);
	cloneTeststeps(model, clonedTestprocess);
	return clonedTestprocess;
    }

    private static void cloneTeststeps(Testprocess model, Testprocess clonedTestprocess) {
	List<Teststep> clonedTeststeps = new ArrayList<Teststep>();
	for (Teststep modelTeststep : model.getTeststeps()) {
	    Teststep clonedTeststep = new TeststepBuilder() //
		    .setTeststepname(modelTeststep.getTeststepname()) //
		    .setParentTestprocess(clonedTestprocess) //
		    .setIrrelevantAllowed(modelTeststep.isIrrelevantAllowed()) //
		    .setFulfilled(false) //
		    .setModel(false) //
		    .build();
	    cloneTestcriteria(modelTeststep, clonedTeststep); //
	    clonedTeststeps.add(clonedTeststep); //
	}
	clonedTestprocess.setTeststeps(clonedTeststeps);
    }

    private static void cloneTestcriteria(Teststep model, Teststep clonedTeststep) {
	List<Testcriterion> clonedTestcriteria = new ArrayList<Testcriterion>();
	for (Testcriterion modelTestcriterion : model.getTestcriteria()) {
	    Testcriterion clonedTestcriterion = new TestcriterionBuilder() //
		    .setTestcriterionname(modelTestcriterion.getTestcriterionname()) //
		    .setParentTeststep(clonedTeststep) //
		    .setModel(false) //
		    .build();
	    cloneTestsubcriteria(modelTestcriterion, clonedTestcriterion);
	    clonedTestcriteria.add(clonedTestcriterion);
	}
	clonedTeststep.setTestcriteria(clonedTestcriteria);
    }

    private static void cloneTestsubcriteria(Testcriterion model, Testcriterion clonedTestcriterion) {
	List<Testsubcriterion> clonedTestsubcriteria = new ArrayList<Testsubcriterion>();
	for (Testsubcriterion modelTestsubcriterion : model.getTestsubcriteria()) {
	    Testsubcriterion clonedTestsubcriterion = new TestsubcriterionBuilder() //
		    .setTestsubcriterionname(modelTestsubcriterion.getTestsubcriterionname()) //
		    .setParentTestcriterion(clonedTestcriterion) //
		    .setScore(modelTestsubcriterion.getScore()) //
		    .setFulfilled(false) //
		    .setIrrelevant(false) //
		    .setModel(false) //
		    .build();
	    clonedTestsubcriteria.add(clonedTestsubcriterion);
	}
	clonedTestcriterion.setTestsubcriteria(clonedTestsubcriteria);
    }
}
