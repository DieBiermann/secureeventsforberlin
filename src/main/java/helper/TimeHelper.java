package helper;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**	A helper class for {@link Date} related functions.
 * 	@author	Annina Biermann
 */
public class TimeHelper {

    /**
     * 	@return	the current time of timezone UTC+01
     */
    public static Date getCurrentTime() {
	TimeZone.setDefault(TimeZone.getTimeZone("Europe/Berlin"));
	return Calendar.getInstance(TimeZone.getDefault()).getTime();
    }
}
