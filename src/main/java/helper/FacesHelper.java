package helper;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import bo.User;
import mb.DataTableManagedBean;
import mb.LoginManagedBean;
import services.FinderService;

/**	Helper class for JSF related functions.
 * 	@author Annina Biermann
 */
public class FacesHelper {

    /**	Gets the clicked element from the {@link HtmlDataTable}
     * 	@see 		DataTableManagedBean
     * 	@param 	clazz	the class, the object from the table is casted to
     * 	@return	the clicked element from the htmlDataTable.
     */
    public static <T> T getSelectedObject(Class<T> clazz) {
	FacesContext context = FacesContext.getCurrentInstance();
	DataTableManagedBean dataTableMB = context.getApplication().evaluateExpressionGet(context,
		"#{dataTableManagedBean}", DataTableManagedBean.class);
	return dataTableMB.getSelectedElement(clazz);
    }

    /**	Finds and returns the user, that is persisted in the {@link LoginManagedBean}.
     * 	@return	the user, that is logged in
     */
    public static User findSessionUser() {
	FacesContext context = FacesContext.getCurrentInstance();
	Integer currentUserId = context.getApplication().evaluateExpressionGet(context,
		"#{loginManagedBean.currentUserId}", Integer.class);
	return FinderService.findById(User.class, currentUserId).get();
    }

    /**	Creates an error faces message on the clientId component.
     * 	@param 	validationMessage	the message to show	
     * 	@param 	clientId				the id of the component, the message is shown in
     */
    public static void createErrorMessage(String validationMessage, String clientId) {
	FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Achtung", validationMessage);
	FacesContext.getCurrentInstance().addMessage(clientId, message);
    }
}
