package jpa;

import javax.persistence.EntityManager;

/**	An interface for running database actions.
 * 	@author	Annina Biermann
 */
public interface Transaction {

    /**	A method, that can run transactions.
     */
    public void run(EntityManager entityManager);
}
