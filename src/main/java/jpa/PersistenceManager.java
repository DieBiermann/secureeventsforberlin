package jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**	The persistence manager, that regulates the creation of {@link EntityManager}s.
 * 	@author	Annina Biermann
 */
public class PersistenceManager {

    /**	The entity manager factory.	*/
    private static EntityManagerFactory emFactory;

    /**	If there is an existing/open entity manager factory, use it for creating a new entity Manager.
     * 	Else, create a new one and use it for creating a new entity Manager.
     * 	@return	An entity manager for accessing the database.
     */
    public static EntityManager getEntityManager() {
	if (emFactory == null || !emFactory.isOpen()) {
	    emFactory = Persistence.createEntityManagerFactory("sefb");
	}
	return emFactory.createEntityManager();
    }

    /**	C ones the current entity manager factory.
     */
    public static void close() {
	emFactory.close();
    }
}