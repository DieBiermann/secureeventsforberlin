package helper;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import bo.Testprocess;
import bo.Teststep;
import builder.TeststepBuilder;
import enums.EditingSuggestion;
import junitHelper.TestObjectHelper;

public class TestprocessHelperTest {

    /** the tested object */
    private Teststep teststep;

    @Before
    public void setup() {
	teststep = TestObjectHelper.createTeststep();
    }

    @Test
    public void test_countFulfilledScore() {

	// No subcriterion is fulfilled or irrelevant.

	Assert.assertEquals(0, TestprocessHelper.countFulfilledScore(teststep));

	// 4 subcriteria are fulfilled, none is irrelevant.

	teststep.getTestcriteria().get(0).getTestsubcriteria().get(0).setFulfilled(true); // score 1
	teststep.getTestcriteria().get(0).getTestsubcriteria().get(2).setFulfilled(true); // score 2
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(3).setFulfilled(true); // score 2
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(5).setFulfilled(true); // score 1

	Assert.assertEquals(6, TestprocessHelper.countFulfilledScore(teststep));

	// 4 subcriteria are fulfilled, 2 of them are irrelevant.

	teststep.getTestcriteria().get(0).getTestsubcriteria().get(2).setIrrelevant(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(5).setIrrelevant(true);

	Assert.assertEquals(3, TestprocessHelper.countFulfilledScore(teststep));

	// 4 subcriteria are fulfilled, 4 of them are irrelevant.

	teststep.getTestcriteria().get(0).getTestsubcriteria().get(0).setIrrelevant(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(3).setIrrelevant(true);

	Assert.assertEquals(0, TestprocessHelper.countFulfilledScore(teststep));
    }

    @Test
    public void test_countRelevantScore() {

	// All subcriteria are relevant

	Assert.assertEquals(22, TestprocessHelper.countRelevantScore(teststep));

	// 4 subcriteria are irrelevant.

	teststep.getTestcriteria().get(0).getTestsubcriteria().get(0).setIrrelevant(true);
	teststep.getTestcriteria().get(0).getTestsubcriteria().get(2).setIrrelevant(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(3).setIrrelevant(true);
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(5).setIrrelevant(true);

	Assert.assertEquals(16, TestprocessHelper.countRelevantScore(teststep));
    }

    @Test
    public void test_calculateTestStepSuggestion() {

	// 13 subcriteria are fulfilled (59%)

	teststep.getTestcriteria().get(0).getTestsubcriteria().get(1).setFulfilled(true); // score 2
	teststep.getTestcriteria().get(0).getTestsubcriteria().get(2).setFulfilled(true); // score 2
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(0).setFulfilled(true); // score 2
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(1).setFulfilled(true); // score 2
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(2).setFulfilled(true); // score 2
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(3).setFulfilled(true); // score 2
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(4).setFulfilled(true); // score 1

	Assert.assertEquals(EditingSuggestion.NO_EDITING, TestprocessHelper.resolveEditingSuggestion(teststep));

	// 14 subcriteria are fulfilled (63%)

	teststep.getTestcriteria().get(1).getTestsubcriteria().get(5).setFulfilled(true); // score 1

	Assert.assertEquals(EditingSuggestion.GENERAL_EDITING, TestprocessHelper.resolveEditingSuggestion(teststep));

	// 17 subcriteria are fulfilled (77%)

	teststep.getTestcriteria().get(1).getTestsubcriteria().get(6).setFulfilled(true); // score 1
	teststep.getTestcriteria().get(1).getTestsubcriteria().get(7).setFulfilled(true); // score 2

	Assert.assertEquals(EditingSuggestion.GENERAL_EDITING, TestprocessHelper.resolveEditingSuggestion(teststep));

	// 18 subcriteria are fulfilled (81%)

	teststep.getTestcriteria().get(1).getTestsubcriteria().get(8).setFulfilled(true); // score 1

	Assert.assertEquals(EditingSuggestion.CONTINUING_EDITING, TestprocessHelper.resolveEditingSuggestion(teststep));
    }

    @Test
    public void test_removeDuplicates() {
	Teststep teststep1 = new TeststepBuilder() //
		.setTeststepname("Teststep 1") //
		.build();

	Teststep teststep2 = new TeststepBuilder() //
		.setTeststepname("Teststep 2") //
		.build();

	Testprocess testprocess = new Testprocess();
	testprocess.setTeststeps(Arrays.asList(teststep1, teststep2, teststep1, teststep2, teststep2, teststep2));

	Testprocess withoutDuplicates = TestprocessHelper.removeDuplicates(testprocess);

	Assert.assertEquals(2, withoutDuplicates.getTeststeps().size());
	Assert.assertEquals(teststep1, withoutDuplicates.getTeststeps().get(0));
	Assert.assertEquals(teststep2, withoutDuplicates.getTeststeps().get(1));
    }
}
