package junitHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import bo.Event;
import bo.Testcriterion;
import bo.Teststep;
import bo.Testsubcriterion;
import builder.TestcriterionBuilder;
import builder.TeststepBuilder;
import builder.TestsubcriterionBuilder;
import enums.EventStatus;

/**	A helper class for jUnit test, that creates fully-fledged test-objects
 * 	@author Annina Biermann
 */
public class TestObjectHelper {

    /**
     * 	@return	A event object without testprocess and user.
     * 	@throws 	ParseException 	When the date parsing fails.
     */
    public static Event createEvent() throws ParseException {
	Event event = new Event();
	event.setEventname("Eine riesen Geburtstagsparty");
	event.setStatus(EventStatus.UNDER_EXAMINATION);
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
	event.setStart(simpleDateFormat.parse("01.01.2001 18:00"));
	event.setEnd(simpleDateFormat.parse("02.01.2001 07:00"));
	return event;
    }

    /** 	Creates a fully-fledged {@link Teststep} with {@link Testcriterion} objects and {@link
    *		Testsubcriterion}.
    * 		@return 	A fully-fledged {@link Teststep} object.
    */
    public static Teststep createTeststep() {

	// CREATES THE CRITERION EVENTCONCEPT AND ITS SUBCRITERIA

	Testcriterion eventconcept = new TestcriterionBuilder() //
		.setTestcriterionname("Veranstaltungskonzept") //
		.build();

	/* # */
	/* 0 */ Testsubcriterion eventData = createTestsubcriterion(eventconcept, "Veranstaltungsdaten", 1);
	/* 1 */ Testsubcriterion eventZone = createTestsubcriterion(eventconcept, "Veranstaltungsbereich", 2);
	/* 2 */ Testsubcriterion visitors = createTestsubcriterion(eventconcept, "Besucher/Teilnehmer", 2);
	/* 3 */ Testsubcriterion eventDescription = createTestsubcriterion(eventconcept, "Veranstaltungsart/Beschreibung", 1);
	/* 4 */ Testsubcriterion eventProcess = createTestsubcriterion(eventconcept, "Veranstaltungsablauf", 1);

	eventconcept.setTestsubcriteria(Arrays.asList(eventData, eventZone, visitors, eventDescription, eventProcess));

	// CREATES THE CRITERION SAFETYCONCEPT

	Testcriterion safetyconcept = new TestcriterionBuilder() //
		.setTestcriterionname("Sicherheitskonzept") //
		.build();

	/* # */
	/* 0 */ Testsubcriterion emergencyRoutes = createTestsubcriterion(safetyconcept, "Rettungswege", 2);
	/* 1 */ Testsubcriterion medicalService = createTestsubcriterion(safetyconcept, "Sanitätsdienst", 2);
	/* 2 */ Testsubcriterion fireProtection = createTestsubcriterion(safetyconcept, "Brandschutz", 2);
	/* 3 */ Testsubcriterion visitorGuiding = createTestsubcriterion(safetyconcept, "Besucherführung", 2);
	/* 4 */ Testsubcriterion constructions = createTestsubcriterion(safetyconcept, "Aufbauten", 1);
	/* 5 */ Testsubcriterion coordinatingPoint = createTestsubcriterion(safetyconcept, "Koordinierungsstelle", 1);
	/* 6 */ Testsubcriterion communication = createTestsubcriterion(safetyconcept, "Kommunikation", 1);
	/* 7 */ Testsubcriterion damageScenario = createTestsubcriterion(safetyconcept, "Schadensszenarien", 2);
	/* 8 */ Testsubcriterion maps = createTestsubcriterion(safetyconcept, "Kartenmaterial", 1);
	/* 9 */ Testsubcriterion parallelEvents = createTestsubcriterion(safetyconcept, "Parallelveranstaltungen", 1);

	safetyconcept.setTestsubcriteria(Arrays.asList(emergencyRoutes, medicalService, fireProtection, visitorGuiding, constructions,
		coordinatingPoint, communication, damageScenario, maps, parallelEvents));

	// CREATES THE TESTSTEP WITH EVENT- AND SAFETYCONCEPT AS CHILDREN

	Teststep teststep = new TeststepBuilder() //
		.setTeststepname("Teil Aa - Vorprüfung") //
		.setTestcriteria(Arrays.asList(eventconcept, safetyconcept)).build();

	eventconcept.setParentTeststep(teststep);

	return teststep;
    }

    private static Testsubcriterion createTestsubcriterion(Testcriterion parent, String name, int score) {
	return new TestsubcriterionBuilder() //
		.setParentTestcriterion(parent) //
		.setTestsubcriterionname(name) //
		.setScore(score) //
		.build();
    }
}
