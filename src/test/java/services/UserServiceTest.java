package services;

import java.util.Arrays;
import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import bo.User;
import enums.Permission;
import provider.ValidationMessageProvider;

/**	An integration test for user functionalities, like login or user creation.
 * 	
 * 	ATTENTION: 	This test runs directly on the database defined in the persistence XML,
 * 	that's why this test should only run on local VMs. The persisted test-content is cleaned up
 * 	after each test, but if an error occurs, there are chances the data in the database may 
 * 	go corrupt.
 *  	@TODO		Implementing this test as a fully-fledged integration tests.
 * 	@author 	Annina Biermann
 */
@Deprecated
public class UserServiceTest {

    /** 	The tested object. */
    private UserService userService;

    /**	test-user-object	*/
    private User user;
    private static final String TEST_USERNAME = "test_username";
    private static final String TEST_PASSWORD = "test_password";

    @Before
    public void setup() {
	userService = new UserService();

	// create test-user
	user = new User();
	user.setUsername(TEST_USERNAME);
	user.setPassword(TEST_PASSWORD);
	user.setPermissions(Arrays.asList(Permission.WRITE));

	// persist test-user
	userService.createUser(user, TEST_PASSWORD);
    }

    @Test
    public void test_hasUserPermission() {
	// user has writing permission.
	Assert.assertTrue(userService.hasUserPermission(user.getId(), Permission.WRITE));
	// user has not administrative permission.
	Assert.assertFalse(userService.hasUserPermission(user.getId(), Permission.ADMINISTRATE));
    }

    // VALIDATION

    @Test
    public void test_validateCredentials() {
	// Valid credentials
	Assert.assertEquals(Optional.empty(), userService.validateCredentials(user.getUsername(), TEST_PASSWORD));

	// Invalid credentials - user exists, but wrong password
	Assert.assertEquals(Optional.of(ValidationMessageProvider.LOGIN_CREDENTIALS_INVALID),
		userService.validateCredentials(user.getUsername(), "wrong_password"));

	// Invalid credentials - user does not exist
	Assert.assertEquals(Optional.of(ValidationMessageProvider.LOGIN_CREDENTIALS_INVALID),
		userService.validateCredentials("wrong_username", TEST_PASSWORD));
    }

    @Test
    public void test_validateNewUserInput() {
	User newUser = new User();
	newUser.setUsername("test_new_username");

	// Valid user
	Assert.assertEquals(Optional.empty(), userService.validateNewUserInput(newUser, TEST_PASSWORD, TEST_PASSWORD));

	// Invalid user - passwords do not match
	Assert.assertEquals(Optional.of(ValidationMessageProvider.USER_PASSWORDS_DONT_MATCH),
		userService.validateNewUserInput(newUser, TEST_PASSWORD, "wrong_password"));
    }

    @Test
    public void test_validateEditedUser() {
	// Valid user
	Assert.assertEquals(Optional.empty(), userService.validateEditedUser(user));

	// Invalid user - username already exists
	User duplicate = new User();
	duplicate.setUsername(TEST_USERNAME);
	Assert.assertEquals(Optional.of(ValidationMessageProvider.USER_ALLREADY_EXISTS), userService.validateEditedUser(duplicate));

	// Invalid user - username does not match pattern
	User wrongPattern = new User();
	wrongPattern.setUsername("a user");
	Assert.assertEquals(Optional.of(ValidationMessageProvider.USERNAME_NO_PATTERNMATCH), userService.validateEditedUser(wrongPattern));
    }

    @After
    public void teardown() {
	userService.deleteUser(user);
    }
}
