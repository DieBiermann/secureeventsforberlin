package services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import bo.Event;
import provider.ValidationMessageProvider;

/**	An integration test for user functionalities, like login or user creation.
 * 	@author 	Annina Biermann
 */
public class EventServiceTest {

    /** the tested object */
    private EventService eventService;

    @Before
    public void setup() {
	eventService = new EventService();
    }

    @Test
    public void test_validateEvent() throws ParseException {
	Event event = new Event();
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

	// Valid dates
	event.setStart(simpleDateFormat.parse("03.02.2010 10:00"));
	event.setEnd(simpleDateFormat.parse("03.02.2010 10:01"));
	Assert.assertEquals(Optional.empty(), eventService.validateEvent(event));

	// Invalid dates - Start and end are the same.
	event.setStart(simpleDateFormat.parse("03.02.2010 10:00"));
	event.setEnd(simpleDateFormat.parse("03.02.2010 10:00"));
	Assert.assertEquals(Optional.of(ValidationMessageProvider.EVENT_END_BEFORE_START), eventService.validateEvent(event));

	// Invalid dates - End lies before start.
	event.setStart(simpleDateFormat.parse("03.02.2010 10:00"));
	event.setEnd(simpleDateFormat.parse("03.02.2010 09:59"));
	Assert.assertEquals(Optional.of(ValidationMessageProvider.EVENT_END_BEFORE_START), eventService.validateEvent(event));
    }
}
